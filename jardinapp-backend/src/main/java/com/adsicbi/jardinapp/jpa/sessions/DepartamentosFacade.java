/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.sessions;

import com.adsicbi.jardinapp.jpa.entities.Departamentos;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andres
 */
@Stateless
public class DepartamentosFacade extends AbstractFacade<Departamentos> {

    @PersistenceContext(unitName = "com.adsicbi.jardinapp_jardinapp-backend_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DepartamentosFacade() {
        super(Departamentos.class);
    }
    
    public List<Departamentos> findByNombre(String nombre) {
        return getEntityManager().createNamedQuery("Departamentos.findByNombreDepartamento")
                .setParameter("nombreDepartamento", nombre + "%")
                .getResultList();
    }
    
}
