/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.sessions;

import com.adsicbi.jardinapp.jpa.entities.Comentarios;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author adsi3
 */
@Stateless
public class ComentariosFacade extends AbstractFacade<Comentarios> {
    @PersistenceContext(unitName = "com.adsicbi.jardinapp_jardinapp-backend_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ComentariosFacade() {
        super(Comentarios.class);
    }

    
    public List<Comentarios> findComentariosByIdJardin(Integer idJardinInfantil) {
        try {
            return  getEntityManager().createNamedQuery("Comentarios.findComentariosByIdJardin")
                    .setParameter("idJardinInantil", idJardinInfantil)
                    .getResultList();
        } catch (NoResultException ex) {
            return null;
        }
    }
    
}
