/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author andres
 */
@Entity
@Table(name = "hijos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hijos.findAll", query = "SELECT h FROM Hijos h"),
    @NamedQuery(name = "Hijos.findByIdHijo", query = "SELECT h FROM Hijos h WHERE h.idHijo = :idHijo"), 
    @NamedQuery(name = "Hijos.findByIdJardinInfantil", query = "SELECT h FROM Hijos h WHERE h.idJardinInfantil.idJardinInfantil = :idJardinInfantil"),
    @NamedQuery(name = "Hijos.idUserLogeado", query = "SELECT h FROM Hijos h WHERE h.idUsuarios.idUsuarios = :idUsuario"),
    @NamedQuery(name = "Hijos.findByPrimerNombre", query = "SELECT h FROM Hijos h WHERE h.primerNombre = :primerNombre"),
    @NamedQuery(name = "Hijos.findBySegundoNombre", query = "SELECT h FROM Hijos h WHERE h.segundoNombre = :segundoNombre"),
    @NamedQuery(name = "Hijos.findByApellidos", query = "SELECT h FROM Hijos h WHERE h.apellidos = :apellidos"),
    @NamedQuery(name = "Hijos.findByRegistroCivil", query = "SELECT h FROM Hijos h WHERE h.registroCivil = :registroCivil")})
public class Hijos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_hijo")
    private Integer idHijo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "primer_nombre")
    private String primerNombre;
    @Size(max = 10)
    @Column(name = "segundo_nombre")
    private String segundoNombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "apellidos")
    private String apellidos;
    @Basic(optional = false)
    @NotNull
    @Column(name = "registro_civil")
    private int registroCivil;
    @Column(name = "estado")
    private Boolean estado =true;
    @JoinColumn(name = "id_usuarios", referencedColumnName = "id_usuarios")
    @ManyToOne(optional = false)
    private Usuarios idUsuarios;
    @JoinColumn(name = "id_jardin_infantil", referencedColumnName = "id_jardin_infantil")
    //@ManyToOne(optional = false)
    private JardinesInfantiles idJardinInfantil;
    @JoinColumn(name = "id_tipo_documento", referencedColumnName = "id_tipo_documento")
    @ManyToOne(optional = false)
    private TiposDocumentos idTipoDocumento;

    public Hijos() {
    }

    public Hijos(Integer idHijo) {
        this.idHijo = idHijo;
    }

    public Hijos(Integer idHijo, String primerNombre, String apellidos, int registroCivil) {
        this.idHijo = idHijo;
        this.primerNombre = primerNombre;
        this.apellidos = apellidos;
        this.registroCivil = registroCivil;
    }

    public Integer getIdHijo() {
        return idHijo;
    }

    public void setIdHijo(Integer idHijo) {
        this.idHijo = idHijo;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getRegistroCivil() {
        return registroCivil;
    }

    public void setRegistroCivil(int registroCivil) {
        this.registroCivil = registroCivil;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Usuarios getIdUsuarios() {
        return idUsuarios;
    }

    public void setIdUsuarios(Usuarios idUsuarios) {
        this.idUsuarios = idUsuarios;
    }

    public JardinesInfantiles getIdJardinInfantil() {
        return idJardinInfantil;
    }

    public void setIdJardinInfantil(JardinesInfantiles idJardinInfantil) {
        this.idJardinInfantil = idJardinInfantil;
    }

    public TiposDocumentos getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(TiposDocumentos idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idHijo != null ? idHijo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hijos)) {
            return false;
        }
        Hijos other = (Hijos) object;
        if ((this.idHijo == null && other.idHijo != null) || (this.idHijo != null && !this.idHijo.equals(other.idHijo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.adsicbi.jardinapp.jpa.entities.Hijos[ idHijo=" + idHijo + " ]";
    }
    
}
