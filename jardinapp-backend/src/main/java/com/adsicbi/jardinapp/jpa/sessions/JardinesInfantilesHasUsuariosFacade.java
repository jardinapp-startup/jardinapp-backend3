/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.sessions;

import com.adsicbi.jardinapp.jpa.entities.JardinesInfantilesHasUsuarios;
import com.adsicbi.jardinapp.jpa.entities.Usuarios;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andres
 */
@Stateless
public class JardinesInfantilesHasUsuariosFacade extends AbstractFacade<JardinesInfantilesHasUsuarios> {

    @PersistenceContext(unitName = "com.adsicbi.jardinapp_jardinapp-backend_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public JardinesInfantilesHasUsuariosFacade() {
        super(JardinesInfantilesHasUsuarios.class);
    }
     public List<JardinesInfantilesHasUsuarios> findByIdUsuario(Integer idUsuario) {
        return getEntityManager().createNamedQuery("JardinesInfantilesHasUsuarios.findByIdUsuario")
                .setParameter("idUsuario", idUsuario )
                .getResultList();
    }
    
     
}
