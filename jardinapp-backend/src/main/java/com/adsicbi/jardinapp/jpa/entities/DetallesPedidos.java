/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paola Andrea
 */
@Entity
@Table(name = "detalles_pedidos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetallesPedidos.findAll", query = "SELECT d FROM DetallesPedidos d"),
    @NamedQuery(name = "DetallesPedidos.findByIdServicio", query = "SELECT d FROM DetallesPedidos d WHERE d.detallesPedidosPK.idServicio = :idServicio"),
    @NamedQuery(name = "DetallesPedidos.findByIdPedido", query = "SELECT d FROM DetallesPedidos d WHERE d.detallesPedidosPK.idPedido = :idPedido"),
    @NamedQuery(name = "DetallesPedidos.findByDuracionMeses", query = "SELECT d FROM DetallesPedidos d WHERE d.duracionMeses = :duracionMeses"),
    @NamedQuery(name = "DetallesPedidos.findByFechaInicio", query = "SELECT d FROM DetallesPedidos d WHERE d.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "DetallesPedidos.findByFechaFin", query = "SELECT d FROM DetallesPedidos d WHERE d.fechaFin = :fechaFin"),
    @NamedQuery(name = "DetallesPedidos.findByIdUsuario", query = "SELECT d FROM DetallesPedidos d WHERE d.pedidos.idUsuarios.idUsuarios = :idUsuarios")})
public class DetallesPedidos implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DetallesPedidosPK detallesPedidosPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "duracion_meses")
    private int duracionMeses;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;
    @JoinColumn(name = "id_pedido", referencedColumnName = "id_pedido", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Pedidos pedidos;
    @JoinColumn(name = "id_servicio", referencedColumnName = "id_servicio", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Servicios servicios;

    public DetallesPedidos() {
    }

    public DetallesPedidos(DetallesPedidosPK detallesPedidosPK) {
        this.detallesPedidosPK = detallesPedidosPK;
    }

    public DetallesPedidos(DetallesPedidosPK detallesPedidosPK, int duracionMeses, Date fechaInicio, Date fechaFin) {
        this.detallesPedidosPK = detallesPedidosPK;
        this.duracionMeses = duracionMeses;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
    }

    public DetallesPedidos(int idServicio, int idPedido) {
        this.detallesPedidosPK = new DetallesPedidosPK(idServicio, idPedido);
    }

    public DetallesPedidosPK getDetallesPedidosPK() {
        return detallesPedidosPK;
    }

    public void setDetallesPedidosPK(DetallesPedidosPK detallesPedidosPK) {
        this.detallesPedidosPK = detallesPedidosPK;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Pedidos getPedidos() {
        return pedidos;
    }

    public void setPedidos(Pedidos pedidos) {
        this.pedidos = pedidos;
    }

    public Servicios getServicios() {
        return servicios;
    }

    public void setServicios(Servicios servicios) {
        this.servicios = servicios;
    }

    public int getDuracionMeses() {
        return duracionMeses;
    }

    public void setDuracionMeses(int duracionMeses) {
        this.duracionMeses = duracionMeses;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (detallesPedidosPK != null ? detallesPedidosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetallesPedidos)) {
            return false;
        }
        DetallesPedidos other = (DetallesPedidos) object;
        if ((this.detallesPedidosPK == null && other.detallesPedidosPK != null) || (this.detallesPedidosPK != null && !this.detallesPedidosPK.equals(other.detallesPedidosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.adsicbi.jardinapp.jpa.entities.DetallesPedidos[ detallesPedidosPK=" + detallesPedidosPK + " ]";
    }
    
}
