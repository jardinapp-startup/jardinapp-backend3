/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Paola Andrea
 */
@Embeddable
public class DetallesPedidosPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_servicio")
    private int idServicio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_pedido")
    private int idPedido;

    public DetallesPedidosPK() {
    }

    public DetallesPedidosPK(int idServicio, int idPedido) {
        this.idServicio = idServicio;
        this.idPedido = idPedido;
    }

    public int getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(int idServicio) {
        this.idServicio = idServicio;
    }

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idServicio;
        hash += (int) idPedido;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetallesPedidosPK)) {
            return false;
        }
        DetallesPedidosPK other = (DetallesPedidosPK) object;
        if (this.idServicio != other.idServicio) {
            return false;
        }
        if (this.idPedido != other.idPedido) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.adsicbi.jardinapp.jpa.entities.DetallesPedidosPK[ idServicio=" + idServicio + ", idPedido=" + idPedido + " ]";
    }
    
}
