/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author APRENDIZ
 */
@Entity
@Table(name = "hijos_au")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HijosAu.findAll", query = "SELECT h FROM HijosAu h"),
    @NamedQuery(name = "HijosAu.findByIdHijo", query = "SELECT h FROM HijosAu h WHERE h.idHijo = :idHijo"),
    @NamedQuery(name = "HijosAu.findByPrimerNombreOld", query = "SELECT h FROM HijosAu h WHERE h.primerNombreOld = :primerNombreOld"),
    @NamedQuery(name = "HijosAu.findBySegundoNombreOld", query = "SELECT h FROM HijosAu h WHERE h.segundoNombreOld = :segundoNombreOld"),
    @NamedQuery(name = "HijosAu.findByApellidosOld", query = "SELECT h FROM HijosAu h WHERE h.apellidosOld = :apellidosOld"),
    @NamedQuery(name = "HijosAu.findByRegistroCivilOld", query = "SELECT h FROM HijosAu h WHERE h.registroCivilOld = :registroCivilOld"),
    @NamedQuery(name = "HijosAu.findByPrimerNombreNew", query = "SELECT h FROM HijosAu h WHERE h.primerNombreNew = :primerNombreNew"),
    @NamedQuery(name = "HijosAu.findBySegundoNombreNew", query = "SELECT h FROM HijosAu h WHERE h.segundoNombreNew = :segundoNombreNew"),
    @NamedQuery(name = "HijosAu.findByApellidosNew", query = "SELECT h FROM HijosAu h WHERE h.apellidosNew = :apellidosNew"),
    @NamedQuery(name = "HijosAu.findByRegistroCivilNew", query = "SELECT h FROM HijosAu h WHERE h.registroCivilNew = :registroCivilNew")})
public class HijosAu implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_hijo")
    private Integer idHijo;
    @Size(max = 15)
    @Column(name = "primer_nombre_old")
    private String primerNombreOld;
    @Size(max = 15)
    @Column(name = "segundo_nombre_old")
    private String segundoNombreOld;
    @Size(max = 45)
    @Column(name = "apellidos_old")
    private String apellidosOld;
    @Column(name = "registro_civil_old")
    private Integer registroCivilOld;
    @Size(max = 15)
    @Column(name = "primer_nombre_new")
    private String primerNombreNew;
    @Size(max = 15)
    @Column(name = "segundo_nombre_new")
    private String segundoNombreNew;
    @Size(max = 45)
    @Column(name = "apellidos_new")
    private String apellidosNew;
    @Column(name = "registro_civil_new")
    private Integer registroCivilNew;

    public HijosAu() {
    }

    public HijosAu(Integer idHijo) {
        this.idHijo = idHijo;
    }

    public Integer getIdHijo() {
        return idHijo;
    }

    public void setIdHijo(Integer idHijo) {
        this.idHijo = idHijo;
    }

    public String getPrimerNombreOld() {
        return primerNombreOld;
    }

    public void setPrimerNombreOld(String primerNombreOld) {
        this.primerNombreOld = primerNombreOld;
    }

    public String getSegundoNombreOld() {
        return segundoNombreOld;
    }

    public void setSegundoNombreOld(String segundoNombreOld) {
        this.segundoNombreOld = segundoNombreOld;
    }

    public String getApellidosOld() {
        return apellidosOld;
    }

    public void setApellidosOld(String apellidosOld) {
        this.apellidosOld = apellidosOld;
    }

    public Integer getRegistroCivilOld() {
        return registroCivilOld;
    }

    public void setRegistroCivilOld(Integer registroCivilOld) {
        this.registroCivilOld = registroCivilOld;
    }

    public String getPrimerNombreNew() {
        return primerNombreNew;
    }

    public void setPrimerNombreNew(String primerNombreNew) {
        this.primerNombreNew = primerNombreNew;
    }

    public String getSegundoNombreNew() {
        return segundoNombreNew;
    }

    public void setSegundoNombreNew(String segundoNombreNew) {
        this.segundoNombreNew = segundoNombreNew;
    }

    public String getApellidosNew() {
        return apellidosNew;
    }

    public void setApellidosNew(String apellidosNew) {
        this.apellidosNew = apellidosNew;
    }

    public Integer getRegistroCivilNew() {
        return registroCivilNew;
    }

    public void setRegistroCivilNew(Integer registroCivilNew) {
        this.registroCivilNew = registroCivilNew;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idHijo != null ? idHijo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HijosAu)) {
            return false;
        }
        HijosAu other = (HijosAu) object;
        if ((this.idHijo == null && other.idHijo != null) || (this.idHijo != null && !this.idHijo.equals(other.idHijo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.adsicbi.jardinapp.jpa.entities.HijosAu[ idHijo=" + idHijo + " ]";
    }
    
}
