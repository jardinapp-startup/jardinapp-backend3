/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.sessions;

import com.adsicbi.jardinapp.jpa.entities.RespuestasInquietudes;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author bratc
 */
@Stateless
public class RespuestasInquietudesFacade extends AbstractFacade<RespuestasInquietudes> {

    @PersistenceContext(unitName = "com.adsicbi.jardinapp_jardinapp-backend_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RespuestasInquietudesFacade() {
        super(RespuestasInquietudes.class);
    }

    public List<RespuestasInquietudes> byInquietudesId(Integer id) {
       
        return getEntityManager().createNamedQuery("RespuestasInquietudes.findByIdInquietud")
                .setParameter("idInquietud", id)
                .getResultList();
    }
}
