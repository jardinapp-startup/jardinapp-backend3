/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.sessions;

import com.adsicbi.jardinapp.jpa.entities.Anuncios;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andres
 */
@Stateless
public class AnunciosFacade extends AbstractFacade<Anuncios> {

    @PersistenceContext(unitName = "com.adsicbi.jardinapp_jardinapp-backend_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AnunciosFacade() {
        super(Anuncios.class);
    }
  
    public List<Anuncios>findAllA(){
        return getEntityManager().createNamedQuery("Anuncios.findAllA").getResultList();
    }
    
    
    public List<Anuncios>findByJardinAdmin(Integer idUsuarios){
        return getEntityManager().createNativeQuery("select distinct a.id_anuncio,a.titulo,a.descripcion,a.fecha_fin,a.fecha_inicio,a.estado, a.id_jardin_infantil"
                + " from anuncios a join jardines_infantiles j join jardines_infantiles_has_usuarios jh join usuarios u on a.id_jardin_infantil = j.id_jardin_infantil and j.id_jardin_infantil = jh.id_jardin_infantil and jh.id_usuarios = "+idUsuarios,Anuncios.class).getResultList();
    }
    public List<Anuncios>findAnunciosByClienteJardin(Integer idPadre){
        return getEntityManager().createNativeQuery("SELECT DISTINCT a.id_anuncio, j.id_jardin_infantil,  a.titulo, a.descripcion, a.fecha_inicio, a.fecha_fin, j.nombre_jardin \n" +
"FROM  hijos h JOIN jardines_infantiles j ON h.id_jardin_infantil=j.id_jardin_infantil\n" +
"JOIN anuncios a  ON a.id_jardin_infantil=j.id_jardin_infantil  \n" +
"WHERE a.estado=1 AND  h.id_usuarios = "+idPadre,Anuncios.class).getResultList();
    }
    
    
}
