/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author andres
 */
@Entity
@Table(name = "jardines_infantiles_AU")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "JardinesinfantilesAU.findAll", query = "SELECT j FROM JardinesinfantilesAU j"),
    @NamedQuery(name = "JardinesinfantilesAU.findByIdTriggerJardines", query = "SELECT j FROM JardinesinfantilesAU j WHERE j.idTriggerJardines = :idTriggerJardines"),
    @NamedQuery(name = "JardinesinfantilesAU.findByNombreJardinOld", query = "SELECT j FROM JardinesinfantilesAU j WHERE j.nombreJardinOld = :nombreJardinOld"),
    @NamedQuery(name = "JardinesinfantilesAU.findByDireccionOld", query = "SELECT j FROM JardinesinfantilesAU j WHERE j.direccionOld = :direccionOld"),
    @NamedQuery(name = "JardinesinfantilesAU.findByCelularOld", query = "SELECT j FROM JardinesinfantilesAU j WHERE j.celularOld = :celularOld"),
    @NamedQuery(name = "JardinesinfantilesAU.findByTelefonoOld", query = "SELECT j FROM JardinesinfantilesAU j WHERE j.telefonoOld = :telefonoOld"),
    @NamedQuery(name = "JardinesinfantilesAU.findByEmailOld", query = "SELECT j FROM JardinesinfantilesAU j WHERE j.emailOld = :emailOld"),
    @NamedQuery(name = "JardinesinfantilesAU.findBySitiowebOld", query = "SELECT j FROM JardinesinfantilesAU j WHERE j.sitiowebOld = :sitiowebOld"),
    @NamedQuery(name = "JardinesinfantilesAU.findByEstadoOld", query = "SELECT j FROM JardinesinfantilesAU j WHERE j.estadoOld = :estadoOld"),
    @NamedQuery(name = "JardinesinfantilesAU.findByNombreJardinNew", query = "SELECT j FROM JardinesinfantilesAU j WHERE j.nombreJardinNew = :nombreJardinNew"),
    @NamedQuery(name = "JardinesinfantilesAU.findByDireccionNew", query = "SELECT j FROM JardinesinfantilesAU j WHERE j.direccionNew = :direccionNew"),
    @NamedQuery(name = "JardinesinfantilesAU.findByCelularNew", query = "SELECT j FROM JardinesinfantilesAU j WHERE j.celularNew = :celularNew"),
    @NamedQuery(name = "JardinesinfantilesAU.findByTelefonoNew", query = "SELECT j FROM JardinesinfantilesAU j WHERE j.telefonoNew = :telefonoNew"),
    @NamedQuery(name = "JardinesinfantilesAU.findByEmailNew", query = "SELECT j FROM JardinesinfantilesAU j WHERE j.emailNew = :emailNew"),
    @NamedQuery(name = "JardinesinfantilesAU.findBySitiowebNew", query = "SELECT j FROM JardinesinfantilesAU j WHERE j.sitiowebNew = :sitiowebNew"),
    @NamedQuery(name = "JardinesinfantilesAU.findByEstadoNew", query = "SELECT j FROM JardinesinfantilesAU j WHERE j.estadoNew = :estadoNew")})
public class JardinesinfantilesAU implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_trigger_jardines")
    private Integer idTriggerJardines;
    @Size(max = 45)
    @Column(name = "nombre_jardin_old")
    private String nombreJardinOld;
    @Size(max = 45)
    @Column(name = "direccion_old")
    private String direccionOld;
    @Size(max = 45)
    @Column(name = "celular_old")
    private String celularOld;
    @Size(max = 45)
    @Column(name = "telefono_old")
    private String telefonoOld;
    @Size(max = 45)
    @Column(name = "email_old")
    private String emailOld;
    @Size(max = 45)
    @Column(name = "sitioweb_old")
    private String sitiowebOld;
    @Size(max = 45)
    @Column(name = "estado_old")
    private String estadoOld;
    @Lob
    @Column(name = "foto_old")
    private byte[] fotoOld;
    @Size(max = 45)
    @Column(name = "nombre_jardin_new")
    private String nombreJardinNew;
    @Size(max = 45)
    @Column(name = "direccion_new")
    private String direccionNew;
    @Size(max = 45)
    @Column(name = "celular_new")
    private String celularNew;
    @Size(max = 45)
    @Column(name = "telefono_new")
    private String telefonoNew;
    @Size(max = 45)
    @Column(name = "email_new")
    private String emailNew;
    @Size(max = 45)
    @Column(name = "sitioweb_new")
    private String sitiowebNew;
    @Size(max = 45)
    @Column(name = "estado_new")
    private String estadoNew;
    @Lob
    @Column(name ="foto_new")
    private byte[] fotoNew;

    public JardinesinfantilesAU() {
    }

    public JardinesinfantilesAU(Integer idTriggerJardines) {
        this.idTriggerJardines = idTriggerJardines;
    }

    public Integer getIdTriggerJardines() {
        return idTriggerJardines;
    }

    public void setIdTriggerJardines(Integer idTriggerJardines) {
        this.idTriggerJardines = idTriggerJardines;
    }

    public String getNombreJardinOld() {
        return nombreJardinOld;
    }

    public void setNombreJardinOld(String nombreJardinOld) {
        this.nombreJardinOld = nombreJardinOld;
    }

    public String getDireccionOld() {
        return direccionOld;
    }

    public void setDireccionOld(String direccionOld) {
        this.direccionOld = direccionOld;
    }

    public String getCelularOld() {
        return celularOld;
    }

    public void setCelularOld(String celularOld) {
        this.celularOld = celularOld;
    }

    public String getTelefonoOld() {
        return telefonoOld;
    }

    public void setTelefonoOld(String telefonoOld) {
        this.telefonoOld = telefonoOld;
    }

    public String getEmailOld() {
        return emailOld;
    }

    public void setEmailOld(String emailOld) {
        this.emailOld = emailOld;
    }

    public String getSitiowebOld() {
        return sitiowebOld;
    }

    public void setSitiowebOld(String sitiowebOld) {
        this.sitiowebOld = sitiowebOld;
    }

    public String getEstadoOld() {
        return estadoOld;
    }

    public void setEstadoOld(String estadoOld) {
        this.estadoOld = estadoOld;
    }
    
    public byte[] getFotoOld() {
        return fotoOld;
    }

    public void setFotoOld(byte[] fotoOld) {
        this.fotoOld = fotoOld;
    }

    public String getNombreJardinNew() {
        return nombreJardinNew;
    }

    public void setNombreJardinNew(String nombreJardinNew) {
        this.nombreJardinNew = nombreJardinNew;
    }

    public String getDireccionNew() {
        return direccionNew;
    }

    public void setDireccionNew(String direccionNew) {
        this.direccionNew = direccionNew;
    }

    public String getCelularNew() {
        return celularNew;
    }

    public void setCelularNew(String celularNew) {
        this.celularNew = celularNew;
    }

    public String getTelefonoNew() {
        return telefonoNew;
    }

    public void setTelefonoNew(String telefonoNew) {
        this.telefonoNew = telefonoNew;
    }

    public String getEmailNew() {
        return emailNew;
    }

    public void setEmailNew(String emailNew) {
        this.emailNew = emailNew;
    }

    public String getSitiowebNew() {
        return sitiowebNew;
    }

    public void setSitiowebNew(String sitiowebNew) {
        this.sitiowebNew = sitiowebNew;
    }

    public String getEstadoNew() {
        return estadoNew;
    }

    public void setEstadoNew(String estadoNew) {
        this.estadoNew = estadoNew;
    }
    
    public byte[] getFotoNew() {
        return fotoNew;
    }

    public void setFotoNew(byte[] fotoNew) {
        this.fotoNew = fotoNew;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTriggerJardines != null ? idTriggerJardines.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JardinesinfantilesAU)) {
            return false;
        }
        JardinesinfantilesAU other = (JardinesinfantilesAU) object;
        if ((this.idTriggerJardines == null && other.idTriggerJardines != null) || (this.idTriggerJardines != null && !this.idTriggerJardines.equals(other.idTriggerJardines))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.adsicbi.jardinapp.jpa.entities.JardinesinfantilesAU[ idTriggerJardines=" + idTriggerJardines + " ]";
    }
    
}
