/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.entities;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bratc
 */
@Entity
@Table(name = "respuestas_inquietudes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RespuestasInquietudes.findAll", query = "SELECT r FROM RespuestasInquietudes r"),
    @NamedQuery(name = "RespuestasInquietudes.findByIdrespuestas", query = "SELECT r FROM RespuestasInquietudes r WHERE r.idrespuestas = :idrespuestas"),
    @NamedQuery(name = "RespuestasInquietudes.findByIdInquietud", query = "SELECT r FROM RespuestasInquietudes r WHERE r.idInquietudes.idInquietud = :idInquietud"),
    @NamedQuery(name = "RespuestasInquietudes.findByRespuesta", query = "SELECT r FROM RespuestasInquietudes r WHERE r.respuesta = :respuesta"),
    @NamedQuery(name = "RespuestasInquietudes.findByFecha", query = "SELECT r FROM RespuestasInquietudes r WHERE r.fecha = :fecha")})
public class RespuestasInquietudes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idrespuestas")
    private Integer idrespuestas;
    @Size(max = 180)
    @Column(name = "respuesta")
    private String respuesta;
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    
    @JoinColumn(name = "id_inquietudes", referencedColumnName = "id_inquietud")
    @ManyToOne(optional = false)
    private Inquietudes idInquietudes;

    @Transient
    SimpleDateFormat date = new SimpleDateFormat("MM/dd/yyyy");

    public RespuestasInquietudes() {
    }

    public RespuestasInquietudes(Integer idrespuestas) {
        this.idrespuestas = idrespuestas;
    }

    public Integer getIdrespuestas() {
        return idrespuestas;
    }

    public void setIdrespuestas(Integer idrespuestas) {
        this.idrespuestas = idrespuestas;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

   public String getFecha() {
        return date.format(fecha);
    }

    public void setFecha(String fecha) {
         this.fecha = new Date();
    }
    public Inquietudes getIdInquietudes() {
        return idInquietudes;
    }

    public void setIdInquietudes(Inquietudes idInquietudes) {
        this.idInquietudes = idInquietudes;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idrespuestas != null ? idrespuestas.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RespuestasInquietudes)) {
            return false;
        }
        RespuestasInquietudes other = (RespuestasInquietudes) object;
        if ((this.idrespuestas == null && other.idrespuestas != null) || (this.idrespuestas != null && !this.idrespuestas.equals(other.idrespuestas))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.adsicbi.jardinapp.jpa.entities.RespuestasInquietudes[ idrespuestas=" + idrespuestas + " ]";
    }
    
}
