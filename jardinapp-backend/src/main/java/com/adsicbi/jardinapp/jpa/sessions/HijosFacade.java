/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.sessions;

import com.adsicbi.jardinapp.jpa.entities.Hijos;
import com.adsicbi.jardinapp.jpa.entities.JardinesInfantilesHasUsuarios;
import com.adsicbi.jardinapp.jpa.entities.Usuarios;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andres
 */
@Stateless
public class HijosFacade extends AbstractFacade<Hijos> {

    @PersistenceContext(unitName = "com.adsicbi.jardinapp_jardinapp-backend_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HijosFacade() {
        super(Hijos.class);
    }
    
       public List<Hijos> findByIdJardinInfantil(Integer idJardinInfantil) {
        return getEntityManager().createNamedQuery("Hijos.findByIdJardinInfantil")
                .setParameter("idJardinInfantil", idJardinInfantil )
                .getResultList();
    } 

    public  List<Hijos> getMisHijos(Integer idUserLogeado) {
        return getEntityManager().createNamedQuery("Hijos.idUserLogeado")
                        .setParameter("idUsuario", idUserLogeado )
                        .getResultList();    

        }

    public Hijos findHijosByRegistroCivil(int registroCivil) {
       try {
            return (Hijos) getEntityManager().createNamedQuery("Hijos.findByRegistroCivil")
                    .setParameter("registroCivil", registroCivil) 
                    .getSingleResult();
        } catch (NonUniqueResultException ex) {
            throw ex;
        } catch (NoResultException ex) {
            return null;
        }
    }

   
}
 