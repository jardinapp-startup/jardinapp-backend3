/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.sessions;


import com.adsicbi.jardinapp.jpa.entities.Hijos;
import com.adsicbi.jardinapp.jpa.entities.JardinesInfantiles;
import com.adsicbi.jardinapp.jpa.entities.Usuarios;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author andres
 */
@Stateless
public class JardinesInfantilesFacade extends AbstractFacade<JardinesInfantiles> {

    @PersistenceContext(unitName = "com.adsicbi.jardinapp_jardinapp-backend_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public JardinesInfantilesFacade() {
        super(JardinesInfantiles.class);
    }

    public List<JardinesInfantiles> findByAdminJardin(Integer admin) {
 
           return getEntityManager().createNativeQuery("SELECT j.* FROM jardines_infantiles j JOIN jardines_infantiles_has_usuarios jhu  \n" +
"  ON jhu.id_jardin_infantil = j.id_jardin_infantil WHERE jhu.id_usuarios = "+admin ,JardinesInfantiles.class).getResultList();
    }

 /*   
public List<JardinesInfantiles> findByAdminJardin(Integer admin) {
    return getEntityManager().createNamedQuery("JardinesInfantiles.findByAdminJardin")
            .setParameter("admin", admin)
            .getResultList();
}
*/
    public List<JardinesInfantiles> findAllPublic() {
        return getEntityManager().createNamedQuery("JardinesInfantiles.findAllPublic").getResultList();
                
    
    }

    public JardinesInfantiles createJardin(JardinesInfantiles jardindesInfantiles) {
        getEntityManager().persist(jardindesInfantiles);
      
        return jardindesInfantiles;
      
    }
    
    public List<Hijos> findByClientes(Integer idJardinInfantil){
   
      return getEntityManager().createNativeQuery("select distinct  h.id_hijo, h.apellidos, h.primer_nombre, h.registro_civil,u.id_usuarios,u.nombres,u.email,u.celular "
                + " from usuarios u join hijos h join jardines_infantiles j  where h.id_usuarios = u.id_usuarios and h.id_jardin_infantil  = "+idJardinInfantil,Hijos.class).getResultList();
                             
                
    }

    public List<JardinesInfantiles> findJardin(String data) {
       /* return getEntityManager().createNativeQuery("select distinct  h.id_hijo, h.apellidos, h.primer_nombre, h.registro_civil,u.id_usuarios,u.nombres,u.email,u.celular "
                + " from usuarios u join hijos h join jardines_infantiles j  where h.id_usuarios = u.id_usuarios and h.id_jardin_infantil  = "+idJardinInfantil,Hijos.class).getResultList();
               */
        return getEntityManager().createNamedQuery("JardinesInfantiles.findByNombreJardin")
                .setParameter("nombreJardin", "%"+ data + "%")
                .getResultList();
        
    }
    
    public JardinesInfantiles findByEmail(String email) {
        try {
            return (JardinesInfantiles) getEntityManager().createNamedQuery("JardinesInfantiles.findByEmail")
                    .setParameter("email", email)
                    .getSingleResult();
        } catch (NonUniqueResultException ex) {
            throw ex;
        } catch (NoResultException ex) {
            return null;
        }
    }
}
 
