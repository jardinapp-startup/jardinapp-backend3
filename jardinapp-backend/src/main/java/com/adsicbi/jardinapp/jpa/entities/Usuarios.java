/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.entities;

import com.adsicbi.jardinapp.rest.auth.DigestUtil;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author andres
 */
@Entity
@Table(name = "usuarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuarios.findAllU", query = "SELECT u FROM Usuarios u WHERE u.estado=1"),
    @NamedQuery(name = "Usuarios.findByIdUsuarios", query = "SELECT u FROM Usuarios u WHERE u.idUsuarios = :idUsuarios"),
    @NamedQuery(name = "Usuarios.findByDocumento", query = "SELECT u FROM Usuarios u WHERE u.documento = :documento"),
   // @NamedQuery(name = "Usuarios.findUsuarioByDocumento", query = "SELECT u FROM Usuarios u WHERE  u.documento = :documento"),
    @NamedQuery(name = "Usuarios.findByNombres", query = "SELECT u FROM Usuarios u WHERE u.nombres = :nombres"),
    @NamedQuery(name = "Usuarios.findByApellidos", query = "SELECT u FROM Usuarios u WHERE u.apellidos = :apellidos"),
    @NamedQuery(name = "Usuarios.findByEmail", query = "SELECT u FROM Usuarios u WHERE u.email = :email"), //AND u.estado=1
    @NamedQuery(name = "Usuarios.findByPassword", query = "SELECT u FROM Usuarios u WHERE u.password = :password"),
    @NamedQuery(name = "Usuarios.findByDireccion", query = "SELECT u FROM Usuarios u WHERE u.direccion = :direccion"),
    @NamedQuery(name = "Usuarios.findByTelefono", query = "SELECT u FROM Usuarios u WHERE u.telefono = :telefono"),
    @NamedQuery(name = "Usuarios.findByCelular", query = "SELECT u FROM Usuarios u WHERE u.celular = :celular"),
    @NamedQuery(name = "Usuarios.findByTituloProfesional", query = "SELECT u FROM Usuarios u WHERE u.tituloProfesional = :tituloProfesional"),
    @NamedQuery(name = "Usuarios.findByEstado", query = "SELECT u FROM Usuarios u WHERE u.estado = :estado")})
public class Usuarios implements Serializable {
    @Lob
    @Column(name = "avatar")
    private byte[] avatar;
    @JoinColumn(name = "id_encuesta", referencedColumnName = "id_encuesta")
    @ManyToOne
    private Encuestas idEncuesta;

    @Column(name = "estado")
    private Boolean estado=true;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUsuarios")
    private List<Empresas> empresasList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_usuarios")
    private Integer idUsuarios;
    @Basic(optional = false)
    @NotNull
    @Column(name = "documento")
    private String documento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "nombres")
    private String nombres;
    @Size(max = 30)
    @Column(name = "apellidos")
    private String apellidos;
    //@Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    //@NotNull
    @Size(min = 1, max = 200)
    @Column(name = "password")
    private String password;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 12)
    @Column(name = "telefono")
    //@Pattern(regexp="[0-9 -]*", message="Use solo números")
    private String telefono;
    @Size(max = 15)
    @Column(name = "celular")
    //@Pattern(regexp="[0-9 -]*", message="Use solo números")
    private String celular;
    @Size(max = 50)
    //@Pattern(regexp="[a-zA-Z ]*", message="Use solo letras")
    @Column(name = "titulo_profesional")
    private String tituloProfesional;
    //************ Modify Yohon Bravo
   // @ManyToMany(mappedBy = "usuariosList")
    @JoinTable(name="roles_has_usuarios",joinColumns={
        @JoinColumn(name="id_usuarios",referencedColumnName = "id_usuarios")},
            inverseJoinColumns={
             @JoinColumn(name = "id_rol", referencedColumnName = "id_rol")})
    @ManyToMany
    private List<Roles> rolesList;
    //*********************+
    /*
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUsuarios")
    private List<Anuncios> anunciosList;*/
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUsuarios")
    private List<Pedidos> pedidosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUsuarios")
    private List<Inquietudes> inquietudesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUsuarios")
    private List<JardinesInfantilesHasUsuarios> jardinesInfantilesHasUsuariosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUsuarios")
    private List<Calificaciones> calificacionesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUsuarios")
    private List<Hijos> hijosList;
    @JoinColumns({
        @JoinColumn(name = "id_ciudad", referencedColumnName = "id_ciudad"),
        @JoinColumn(name = "id_departamento", referencedColumnName = "id_departamento")})
    @ManyToOne(optional = false)
    private Ciudades ciudades;
    @JoinColumn(name = "id_tipo_documento", referencedColumnName = "id_tipo_documento")
    @ManyToOne(optional = false)
    private TiposDocumentos idTipoDocumento;
    /*Comentarios Luis*/
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUsuarios")
    private List<Comentarios> comentariosList;

    
    public Usuarios() {
    }

    public Usuarios(Integer idUsuarios) {
        this.idUsuarios = idUsuarios;
    }

    public Usuarios(Integer idUsuarios, String documento, String nombres, String email, String password, String direccion, boolean estado) {
        this.idUsuarios = idUsuarios;
        this.documento = documento;
        this.nombres = nombres;
        this.email = email;
        this.password = password;
        this.direccion = direccion;
        this.estado = estado;
    }


    public Integer getIdUsuarios() {
        return idUsuarios;
    }

    public void setIdUsuarios(Integer idUsuarios) {
        this.idUsuarios = idUsuarios;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }


    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

  //Metodo adaptado para que use DigesUtil para validacion de usuario y generar el hast de password
    public void setPassword(String password) {
        this.password=password;
        /*
        System.out.println("Pass Http "+password);
        try {
            this.password = DigestUtil.generateDigest(password);
            System.out.println(this.password);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
        }*/
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getTituloProfesional() {
        return tituloProfesional;
    }

    public void setTituloProfesional(String tituloProfesional) {
        this.tituloProfesional = tituloProfesional;
    }

    public List<Roles> getRolesList() {
        return rolesList;
    }

    public void setRolesList(List<Roles> rolesList) {
        this.rolesList = rolesList;
    }

   
    @XmlTransient
    public List<Pedidos> getPedidosList() {
        return pedidosList;
    }

    public void setPedidosList(List<Pedidos> pedidosList) {
        this.pedidosList = pedidosList;
    }

    @XmlTransient
    public List<Inquietudes> getInquietudesList() {
        return inquietudesList;
    }

    public void setInquietudesList(List<Inquietudes> inquietudesList) {
        this.inquietudesList = inquietudesList;
    }

    @XmlTransient
    public List<JardinesInfantilesHasUsuarios> getJardinesInfantilesHasUsuariosList() {
        return jardinesInfantilesHasUsuariosList;
    }

    public void setJardinesInfantilesHasUsuariosList(List<JardinesInfantilesHasUsuarios> jardinesInfantilesHasUsuariosList) {
        this.jardinesInfantilesHasUsuariosList = jardinesInfantilesHasUsuariosList;
    }

    @XmlTransient
    public List<Calificaciones> getCalificacionesList() {
        return calificacionesList;
    }

    public void setCalificacionesList(List<Calificaciones> calificacionesList) {
        this.calificacionesList = calificacionesList;
    }

    @XmlTransient
    public List<Hijos> getHijosList() {
        return hijosList;
    }

    public void setHijosList(List<Hijos> hijosList) {
        this.hijosList = hijosList;
    }

    public Ciudades getCiudades() {
        return ciudades;
    }

    public void setCiudades(Ciudades ciudades) {
        this.ciudades = ciudades;
    }

    public TiposDocumentos getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(TiposDocumentos idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }
    
    /*Comentarios Luis*/
    @XmlTransient
    public List<Comentarios> getComentariosList() {
        return comentariosList;
    }

    public void setComentariosList(List<Comentarios> comentariosList) {
        this.comentariosList = comentariosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuarios != null ? idUsuarios.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuarios)) {
            return false;
        }
        Usuarios other = (Usuarios) object;
        if ((this.idUsuarios == null && other.idUsuarios != null) || (this.idUsuarios != null && !this.idUsuarios.equals(other.idUsuarios))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.adsicbi.jardinapp.jpa.entities.Usuarios[ idUsuarios=" + idUsuarios + " ]";
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    @XmlTransient
    public List<Empresas> getEmpresasList() {
        return empresasList;
    }

    public void setEmpresasList(List<Empresas> empresasList) {
        this.empresasList = empresasList;
    }


    public Encuestas getIdEncuesta() {
        return idEncuesta;
    }

    public void setIdEncuesta(Encuestas idEncuesta) {
        this.idEncuesta = idEncuesta;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }
    
}
