/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.sessions;

import com.adsicbi.jardinapp.jpa.entities.TiposDocumentos;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andres
 */
@Stateless
public class TiposDocumentosFacade extends AbstractFacade<TiposDocumentos> {

    @PersistenceContext(unitName = "com.adsicbi.jardinapp_jardinapp-backend_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TiposDocumentosFacade() {
        super(TiposDocumentos.class);
    }
    
     public List<TiposDocumentos> findByNombre(String query){
        return getEntityManager().createNamedQuery("TiposDocumentos.findByNombreTipoDocumento")
                .setParameter("nombreTipoDocumento", query + "%")
                .setMaxResults(10)
                .getResultList();
    }
    
}
