/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.sessions;

import com.adsicbi.jardinapp.jpa.entities.DetallesPedidos;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andres
 */
@Stateless
public class DetallesPedidosFacade extends AbstractFacade<DetallesPedidos> {

    @PersistenceContext(unitName = "com.adsicbi.jardinapp_jardinapp-backend_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DetallesPedidosFacade() {
        super(DetallesPedidos.class);
    }
    
    public List<DetallesPedidos>findByNombre(String nombre){
        return getEntityManager().createNamedQuery("DetallesPedidos.findByNombres")
                .setParameter("nombres", nombre+"%")
                .getResultList();
    }
    
    public List<DetallesPedidos>findByIdUsuario(int idUsuarios){
        return getEntityManager().createNamedQuery("DetallesPedidos.findByIdUsuario")
                .setParameter("idUsuarios", idUsuarios)
                .getResultList();
    }
}
