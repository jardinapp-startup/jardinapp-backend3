/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.sessions;

import com.adsicbi.jardinapp.jpa.entities.Inquietudes;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andres
 */
@Stateless
public class InquietudesFacade extends AbstractFacade<Inquietudes> {

    @PersistenceContext(unitName = "com.adsicbi.jardinapp_jardinapp-backend_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public InquietudesFacade() {
        super(Inquietudes.class);
    }

    public List<Inquietudes> findByAdminJardin(String IdUsuario) {
        return getEntityManager().createNamedQuery("Inquietudes.findByAdminJardin")
                .setParameter("adminjardin",IdUsuario +"%")
                .setMaxResults(10)
                .getResultList();
    }
    

    public List<Inquietudes> findByidJardin(Integer idUsuario) {
        /*return getEntityManager().createNamedQuery("Inquietudes.findByidUsuario")
                .setParameter("idUsuario", idUsuario)
            r    .getResultList();*/
        return getEntityManager().createNativeQuery("select * from inquietudes i\n" +
"join jardines_infantiles j\n" +
"on  j.id_jardin_infantil=i.id_jardin_infantil\n" +
"join jardines_infantiles_has_usuarios jh\n" +
"on jh.id_jardin_infantil=j.id_jardin_infantil\n" +
"where jh.id_usuarios = "+idUsuario,Inquietudes.class).getResultList();
    }
    
    public List<Inquietudes>findByIdUsuarios(int idUsuarios){
        return getEntityManager().createNamedQuery("Inquietudes.findByIdUsuarios")
                .setParameter("idUsuarios", idUsuarios)
                .getResultList();
    }
}
