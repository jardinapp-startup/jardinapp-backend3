/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author andres
 */
@Entity
@Table(name = "anuncios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Anuncios.findAllA", query = "SELECT a FROM Anuncios a WHERE a.estado=1"),
    @NamedQuery(name = "Anuncios.findByIdAnuncio", query = "SELECT a FROM Anuncios a WHERE a.idAnuncio = :idAnuncio"),
    @NamedQuery(name = "Anuncios.findByTitulo", query = "SELECT a FROM Anuncios a WHERE a.titulo = :titulo"),
    @NamedQuery(name = "Anuncios.findByFechaInicio", query = "SELECT a FROM Anuncios a WHERE a.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "Anuncios.findByFechaFin", query = "SELECT a FROM Anuncios a WHERE a.fechaFin = :fechaFin"),
    @NamedQuery(name = "Anuncios.findByEstado", query = "SELECT a FROM Anuncios a WHERE a.estado = :estado")}) 
       
public class Anuncios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_anuncio")
    private Integer idAnuncio;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "titulo")
    private String titulo; 
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private boolean estado=true;
    /*
    @JoinColumn(name = "id_usuarios", referencedColumnName = "id_usuarios")
    @ManyToOne(optional = false)
    private Usuarios idUsuarios;*/
    @JoinColumn(name = "id_jardin_infantil", referencedColumnName = "id_jardin_infantil")
    @ManyToOne(optional = false)
    private JardinesInfantiles idJardinInfantil;
    
    public Anuncios() {
    }

    public Anuncios(Integer idAnuncio) {
        this.idAnuncio = idAnuncio;
    }

    public Anuncios(Integer idAnuncio, String titulo, String descripcion, Date fechaInicio, Date fechaFin, boolean estado) {
        this.idAnuncio = idAnuncio;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.estado = estado;
    }

    public Integer getIdAnuncio() {
        return idAnuncio;
    }

    public void setIdAnuncio(Integer idAnuncio) {
        this.idAnuncio = idAnuncio;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

   

    public JardinesInfantiles getIdJardinInfantil() {
        return idJardinInfantil;
    }

    public void setIdJardinInfantil(JardinesInfantiles idJardinInfantil) {
        this.idJardinInfantil = idJardinInfantil;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAnuncio != null ? idAnuncio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Anuncios)) {
            return false;
        }
        Anuncios other = (Anuncios) object;
        if ((this.idAnuncio == null && other.idAnuncio != null) || (this.idAnuncio != null && !this.idAnuncio.equals(other.idAnuncio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.adsicbi.jardinapp.jpa.entities.Anuncios[ idAnuncio=" + idAnuncio + " ]";
    }
    
}
