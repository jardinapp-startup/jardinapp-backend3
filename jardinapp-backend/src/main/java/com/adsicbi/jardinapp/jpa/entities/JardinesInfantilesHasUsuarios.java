/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author andres
 */
@Entity
@Table(name = "jardines_infantiles_has_usuarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "JardinesInfantilesHasUsuarios.findAll", query = "SELECT j FROM JardinesInfantilesHasUsuarios j"),
    @NamedQuery(name = "JardinesInfantilesHasUsuarios.findByIdUsuario", query = "SELECT j FROM JardinesInfantilesHasUsuarios j WHERE j.idUsuarios.idUsuarios = :idUsuario"),
    @NamedQuery(name = "JardinesInfantilesHasUsuarios.findByIdJardinesHasUsuarios", query = "SELECT j FROM JardinesInfantilesHasUsuarios j WHERE j.idJardinesHasUsuarios = :idJardinesHasUsuarios")})
public class JardinesInfantilesHasUsuarios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_jardines_has_usuarios")
    private Integer idJardinesHasUsuarios;
    @JoinColumn(name = "id_usuarios", referencedColumnName = "id_usuarios")
    @ManyToOne(optional = false)
    private Usuarios idUsuarios;
    @JoinColumn(name = "id_jardin_infantil", referencedColumnName = "id_jardin_infantil")
    @ManyToOne(optional = false)
    private JardinesInfantiles idJardinInfantil;

    public JardinesInfantilesHasUsuarios() {
    }

    public JardinesInfantilesHasUsuarios(Integer idJardinesHasUsuarios) {
        this.idJardinesHasUsuarios = idJardinesHasUsuarios;
    }

    public Integer getIdJardinesHasUsuarios() {
        return idJardinesHasUsuarios;
    }

    public void setIdJardinesHasUsuarios(Integer idJardinesHasUsuarios) {
        this.idJardinesHasUsuarios = idJardinesHasUsuarios;
    }

    public Usuarios getIdUsuarios() {
        return idUsuarios;
    }

    public void setIdUsuarios(Usuarios idUsuarios) {
        this.idUsuarios = idUsuarios;
    }

    public JardinesInfantiles getIdJardinInfantil() {
        return idJardinInfantil;
    }

    public void setIdJardinInfantil(JardinesInfantiles idJardinInfantil) {
        this.idJardinInfantil = idJardinInfantil;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idJardinesHasUsuarios != null ? idJardinesHasUsuarios.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JardinesInfantilesHasUsuarios)) {
            return false;
        }
        JardinesInfantilesHasUsuarios other = (JardinesInfantilesHasUsuarios) object;
        if ((this.idJardinesHasUsuarios == null && other.idJardinesHasUsuarios != null) || (this.idJardinesHasUsuarios != null && !this.idJardinesHasUsuarios.equals(other.idJardinesHasUsuarios))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.adsicbi.jardinapp.jpa.entities.JardinesInfantilesHasUsuarios[ idJardinesHasUsuarios=" + idJardinesHasUsuarios + " ]";
    }
    
}
