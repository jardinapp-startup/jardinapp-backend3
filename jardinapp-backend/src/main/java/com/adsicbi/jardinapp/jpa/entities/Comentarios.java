/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.entities;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author adsi3
 */
@Entity
@Table(name = "comentarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Comentarios.findAll", query = "SELECT c FROM Comentarios c"),
    @NamedQuery(name = "Comentarios.findByIdComentarios", query = "SELECT c FROM Comentarios c WHERE c.idComentarios = :idComentarios"),
    @NamedQuery(name = "Comentarios.findComentariosByIdJardin", query = "SELECT c FROM Comentarios c WHERE c.idJardinInfantil.idJardinInfantil = :idJardinInantil"),
    @NamedQuery(name = "Comentarios.findByDescripcion", query = "SELECT c FROM Comentarios c WHERE c.descripcion = :descripcion"),
    @NamedQuery(name = "Comentarios.findByFecha", query = "SELECT c FROM Comentarios c WHERE c.fecha = :fecha")})
public class Comentarios implements Serializable{
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_comentarios")
    private Integer idComentarios;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @JoinColumn(name = "id_jardin_infantil", referencedColumnName = "id_jardin_infantil")
    @ManyToOne(optional = false)
    private JardinesInfantiles idJardinInfantil;
    @JoinColumn(name = "id_usuarios", referencedColumnName = "id_usuarios")
    @ManyToOne(optional = false)
    private Usuarios idUsuarios;

    @Transient
    SimpleDateFormat date = new SimpleDateFormat("MM/dd/yyyy");
    
    public Comentarios() {
    }

    public Comentarios(Integer idComentarios) {
        this.idComentarios = idComentarios;
    }

    public Comentarios(Integer idComentarios, String descripcion, Date fecha) {
        this.idComentarios = idComentarios;
        this.descripcion = descripcion;
        this.fecha = fecha;
    }

    public Integer getIdComentarios() {
        return idComentarios;
    }

    public void setIdComentarios(Integer idComentarios) {
        this.idComentarios = idComentarios;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha() {
        return date.format(fecha);
    }

    public void setFecha(String fecha) {
        this.fecha = new Date();
    }

    public JardinesInfantiles getIdJardinInfantil() {
        return idJardinInfantil;
    }

    public void setIdJardinInfantil(JardinesInfantiles idJardinInfantil) {
        this.idJardinInfantil = idJardinInfantil;
    }

    public Usuarios getIdUsuarios() {
        return idUsuarios;
    }

    public void setIdUsuarios(Usuarios idUsuarios) {
        this.idUsuarios = idUsuarios;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idComentarios != null ? idComentarios.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Comentarios)) {
            return false;
        }
        Comentarios other = (Comentarios) object;
        if ((this.idComentarios == null && other.idComentarios != null) || (this.idComentarios != null && !this.idComentarios.equals(other.idComentarios))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.adsicbi.jardinapp.jpa.entities.Comentarios[ idComentarios=" + idComentarios + " ]";
    }
    
}
