/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author andres
 */
@Entity
@Table(name = "usuarios_AU")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsuariosAU.findAll", query = "SELECT u FROM UsuariosAU u"),
    @NamedQuery(name = "UsuariosAU.findByIdUsuarios", query = "SELECT u FROM UsuariosAU u WHERE u.idUsuarios = :idUsuarios"),
    @NamedQuery(name = "UsuariosAU.findByDocumentoOld", query = "SELECT u FROM UsuariosAU u WHERE u.documentoOld = :documentoOld"),
    @NamedQuery(name = "UsuariosAU.findByNombresOld", query = "SELECT u FROM UsuariosAU u WHERE u.nombresOld = :nombresOld"),
    @NamedQuery(name = "UsuariosAU.findByApellidosOld", query = "SELECT u FROM UsuariosAU u WHERE u.apellidosOld = :apellidosOld"),
    @NamedQuery(name = "UsuariosAU.findByEmailOld", query = "SELECT u FROM UsuariosAU u WHERE u.emailOld = :emailOld"),
    @NamedQuery(name = "UsuariosAU.findByPasswordOld", query = "SELECT u FROM UsuariosAU u WHERE u.passwordOld = :passwordOld"),
    @NamedQuery(name = "UsuariosAU.findByDireccionOld", query = "SELECT u FROM UsuariosAU u WHERE u.direccionOld = :direccionOld"),
    @NamedQuery(name = "UsuariosAU.findByTelefonoOld", query = "SELECT u FROM UsuariosAU u WHERE u.telefonoOld = :telefonoOld"),
    @NamedQuery(name = "UsuariosAU.findByCelularOld", query = "SELECT u FROM UsuariosAU u WHERE u.celularOld = :celularOld"),
    @NamedQuery(name = "UsuariosAU.findByTituloProfesionalOld", query = "SELECT u FROM UsuariosAU u WHERE u.tituloProfesionalOld = :tituloProfesionalOld"),
    @NamedQuery(name = "UsuariosAU.findByDocumentoNew", query = "SELECT u FROM UsuariosAU u WHERE u.documentoNew = :documentoNew"),
    @NamedQuery(name = "UsuariosAU.findByNombresNew", query = "SELECT u FROM UsuariosAU u WHERE u.nombresNew = :nombresNew"),
    @NamedQuery(name = "UsuariosAU.findByApellidosNew", query = "SELECT u FROM UsuariosAU u WHERE u.apellidosNew = :apellidosNew"),
    @NamedQuery(name = "UsuariosAU.findByEmailNew", query = "SELECT u FROM UsuariosAU u WHERE u.emailNew = :emailNew"),
    @NamedQuery(name = "UsuariosAU.findByPasswordNew", query = "SELECT u FROM UsuariosAU u WHERE u.passwordNew = :passwordNew"),
    @NamedQuery(name = "UsuariosAU.findByDireccionNew", query = "SELECT u FROM UsuariosAU u WHERE u.direccionNew = :direccionNew"),
    @NamedQuery(name = "UsuariosAU.findByTelefonoNew", query = "SELECT u FROM UsuariosAU u WHERE u.telefonoNew = :telefonoNew"),
    @NamedQuery(name = "UsuariosAU.findByCelularNew", query = "SELECT u FROM UsuariosAU u WHERE u.celularNew = :celularNew"),
    @NamedQuery(name = "UsuariosAU.findByTituloProfesionalNew", query = "SELECT u FROM UsuariosAU u WHERE u.tituloProfesionalNew = :tituloProfesionalNew")})
public class UsuariosAU implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_usuarios")
    private Integer idUsuarios;
    @Basic(optional = false)
    @Column(name = "documento_old")
    private int documentoOld;
    @Basic(optional = false)
    @Size(min = 1, max = 30)
    @Column(name = "nombres_old")
    private String nombresOld;
    @Basic(optional = false)
    @Size(min = 1, max = 30)
    @Column(name = "apellidos_old")
    private String apellidosOld;
    @Basic(optional = false)
    @Size(min = 1, max = 50)
    @Column(name = "email_old")
    private String emailOld;
    @Basic(optional = false)
    @Size(min = 1, max = 200)
    @Column(name = "password_old")
    private String passwordOld;
    @Basic(optional = false)
    @Size(min = 1, max = 30)
    @Column(name = "direccion_old")
    private String direccionOld;
    @Basic(optional = false)
    @Size(min = 1, max = 12)
    @Column(name = "telefono_old")
    private String telefonoOld;
    @Basic(optional = false)
    @Size(min = 1, max = 15)
    @Column(name = "celular_old")
    private String celularOld;
    @Basic(optional = false)
    @Size(min = 1, max = 50)
    @Column(name = "titulo_profesional_old")
    private String tituloProfesionalOld;
    @Basic(optional = false)
    @Column(name = "documento_new")
    private int documentoNew;
    @Basic(optional = false)
    @Size(min = 1, max = 30)
    @Column(name = "nombres_new")
    private String nombresNew;
    @Basic(optional = false)
    @Size(min = 1, max = 30)
    @Column(name = "apellidos_new")
    private String apellidosNew;
    @Basic(optional = false)
    @Size(min = 1, max = 50)
    @Column(name = "email_new")
    private String emailNew;
    @Basic(optional = false)
    @Size(min = 1, max = 200)
    @Column(name = "password_new")
    private String passwordNew;
    @Basic(optional = false)
    @Size(min = 1, max = 30)
    @Column(name = "direccion_new")
    private String direccionNew;
    @Basic(optional = false)
    @Size(min = 1, max = 12)
    @Column(name = "telefono_new")
    private String telefonoNew;
    @Basic(optional = false)
    @Size(min = 1, max = 15)
    @Column(name = "celular_new")
    private String celularNew;
    @Basic(optional = false)
    @Size(min = 1, max = 50)
    @Column(name = "titulo_profesional_new")
    private String tituloProfesionalNew;

    public UsuariosAU() {
    }

    public UsuariosAU(Integer idUsuarios) {
        this.idUsuarios = idUsuarios;
    }

    public UsuariosAU(Integer idUsuarios, int documentoOld, String nombresOld, String apellidosOld, String emailOld, String passwordOld, String direccionOld, String telefonoOld, String celularOld, String tituloProfesionalOld, int documentoNew, String nombresNew, String apellidosNew, String emailNew, String passwordNew, String direccionNew, String telefonoNew, String celularNew, String tituloProfesionalNew) {
        this.idUsuarios = idUsuarios;
        this.documentoOld = documentoOld;
        this.nombresOld = nombresOld;
        this.apellidosOld = apellidosOld;
        this.emailOld = emailOld;
        this.passwordOld = passwordOld;
        this.direccionOld = direccionOld;
        this.telefonoOld = telefonoOld;
        this.celularOld = celularOld;
        this.tituloProfesionalOld = tituloProfesionalOld;
        this.documentoNew = documentoNew;
        this.nombresNew = nombresNew;
        this.apellidosNew = apellidosNew;
        this.emailNew = emailNew;
        this.passwordNew = passwordNew;
        this.direccionNew = direccionNew;
        this.telefonoNew = telefonoNew;
        this.celularNew = celularNew;
        this.tituloProfesionalNew = tituloProfesionalNew;
    }

    public Integer getIdUsuarios() {
        return idUsuarios;
    }

    public void setIdUsuarios(Integer idUsuarios) {
        this.idUsuarios = idUsuarios;
    }

    public int getDocumentoOld() {
        return documentoOld;
    }

    public void setDocumentoOld(int documentoOld) {
        this.documentoOld = documentoOld;
    }

    public String getNombresOld() {
        return nombresOld;
    }

    public void setNombresOld(String nombresOld) {
        this.nombresOld = nombresOld;
    }

    public String getApellidosOld() {
        return apellidosOld;
    }

    public void setApellidosOld(String apellidosOld) {
        this.apellidosOld = apellidosOld;
    }

    public String getEmailOld() {
        return emailOld;
    }

    public void setEmailOld(String emailOld) {
        this.emailOld = emailOld;
    }

    public String getPasswordOld() {
        return passwordOld;
    }

    public void setPasswordOld(String passwordOld) {
        this.passwordOld = passwordOld;
    }

    public String getDireccionOld() {
        return direccionOld;
    }

    public void setDireccionOld(String direccionOld) {
        this.direccionOld = direccionOld;
    }

    public String getTelefonoOld() {
        return telefonoOld;
    }

    public void setTelefonoOld(String telefonoOld) {
        this.telefonoOld = telefonoOld;
    }

    public String getCelularOld() {
        return celularOld;
    }

    public void setCelularOld(String celularOld) {
        this.celularOld = celularOld;
    }

    public String getTituloProfesionalOld() {
        return tituloProfesionalOld;
    }

    public void setTituloProfesionalOld(String tituloProfesionalOld) {
        this.tituloProfesionalOld = tituloProfesionalOld;
    }

    public int getDocumentoNew() {
        return documentoNew;
    }

    public void setDocumentoNew(int documentoNew) {
        this.documentoNew = documentoNew;
    }

    public String getNombresNew() {
        return nombresNew;
    }

    public void setNombresNew(String nombresNew) {
        this.nombresNew = nombresNew;
    }

    public String getApellidosNew() {
        return apellidosNew;
    }

    public void setApellidosNew(String apellidosNew) {
        this.apellidosNew = apellidosNew;
    }

    public String getEmailNew() {
        return emailNew;
    }

    public void setEmailNew(String emailNew) {
        this.emailNew = emailNew;
    }

    public String getPasswordNew() {
        return passwordNew;
    }

    public void setPasswordNew(String passwordNew) {
        this.passwordNew = passwordNew;
    }

    public String getDireccionNew() {
        return direccionNew;
    }

    public void setDireccionNew(String direccionNew) {
        this.direccionNew = direccionNew;
    }

    public String getTelefonoNew() {
        return telefonoNew;
    }

    public void setTelefonoNew(String telefonoNew) {
        this.telefonoNew = telefonoNew;
    }

    public String getCelularNew() {
        return celularNew;
    }

    public void setCelularNew(String celularNew) {
        this.celularNew = celularNew;
    }

    public String getTituloProfesionalNew() {
        return tituloProfesionalNew;
    }

    public void setTituloProfesionalNew(String tituloProfesionalNew) {
        this.tituloProfesionalNew = tituloProfesionalNew;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuarios != null ? idUsuarios.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuariosAU)) {
            return false;
        }
        UsuariosAU other = (UsuariosAU) object;
        if ((this.idUsuarios == null && other.idUsuarios != null) || (this.idUsuarios != null && !this.idUsuarios.equals(other.idUsuarios))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.adsicbi.jardinapp.jpa.entities.UsuariosAU[ idUsuarios=" + idUsuarios + " ]";
    }
    
}
