/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.entities;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author andres
 */
@Entity
@Table(name = "inquietudes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Inquietudes.findAll", query = "SELECT i FROM Inquietudes i"),
    @NamedQuery(name = "Inquietudes.findByIdInquietud", query = "SELECT i FROM Inquietudes i WHERE i.idInquietud = :idInquietud"),
    @NamedQuery(name = "Inquietudes.findByIdUsuarios", query = "SELECT i FROM Inquietudes i WHERE i.idUsuarios.idUsuarios = :idUsuarios"),
    @NamedQuery(name = "Inquietudes.findByAsunto", query = "SELECT i FROM Inquietudes i WHERE i.asunto = :asunto"),
    @NamedQuery(name = "Inquietudes.findByDescripcion", query = "SELECT i FROM Inquietudes i WHERE i.descripcion = :descripcion"),
    @NamedQuery(name = "Inquietudes.findByFecha", query = "SELECT i FROM Inquietudes i WHERE i.fecha = :fecha")})
public class Inquietudes implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idInquietudes")
    private List<RespuestasInquietudes> respuestasInquietudesList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_inquietud")
    private Integer idInquietud;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "asunto")
    private String asunto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "descripcion")
    private String descripcion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    
    @JoinColumn(name = "id_usuarios", referencedColumnName = "id_usuarios")
    @ManyToOne(optional = false)
    private Usuarios idUsuarios;
    
    @JoinColumn(name = "id_jardin_infantil", referencedColumnName = "id_jardin_infantil")
    @ManyToOne(optional = false)
    private JardinesInfantiles idJardinInfantil;
    
    @Transient
    SimpleDateFormat date = new SimpleDateFormat("MM/dd/yyyy");

    public Inquietudes() {
    }

    public Inquietudes(Integer idInquietud) {
        this.idInquietud = idInquietud;
    }

    public Inquietudes(Integer idInquietud, String asunto, String descripcion, Date fecha) {
        this.idInquietud = idInquietud;
        this.asunto = asunto;
        this.descripcion = descripcion;
        this.fecha = fecha;
    }

    public Integer getIdInquietud() {
        return idInquietud;
    }

    public void setIdInquietud(Integer idInquietud) {
        this.idInquietud = idInquietud;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha() {
        return date.format(fecha);
    }

    public void setFecha(String fecha) {
         this.fecha = new Date();
    }

    public Usuarios getIdUsuarios() {
        return idUsuarios;
    }

    public void setIdUsuarios(Usuarios idUsuarios) {
        this.idUsuarios = idUsuarios;
    }

    public JardinesInfantiles getIdJardinInfantil() {
        return idJardinInfantil;
    }

    public void setIdJardinInfantil(JardinesInfantiles idJardinInfantil) {
        this.idJardinInfantil = idJardinInfantil;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idInquietud != null ? idInquietud.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inquietudes)) {
            return false;
        }
        Inquietudes other = (Inquietudes) object;
        if ((this.idInquietud == null && other.idInquietud != null) || (this.idInquietud != null && !this.idInquietud.equals(other.idInquietud))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.adsicbi.jardinapp.jpa.entities.Inquietudes[ idInquietud=" + idInquietud + " ]";
    }

    @XmlTransient
    public List<RespuestasInquietudes> getRespuestasInquietudesList() {
        return respuestasInquietudesList;
    }

    public void setRespuestasInquietudesList(List<RespuestasInquietudes> respuestasInquietudesList) {
        this.respuestasInquietudesList = respuestasInquietudesList;
    }
    
}
