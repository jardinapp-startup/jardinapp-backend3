/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author andres
 */
@Entity
@Table(name = "mi_empresa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MiEmpresa.findAll", query = "SELECT m FROM MiEmpresa m"),
    @NamedQuery(name = "MiEmpresa.findByIdMiEmpresa", query = "SELECT m FROM MiEmpresa m WHERE m.idMiEmpresa = :idMiEmpresa"),
    @NamedQuery(name = "MiEmpresa.findByNit", query = "SELECT m FROM MiEmpresa m WHERE m.nit = :nit"),
    @NamedQuery(name = "MiEmpresa.findByNombreEmpresa", query = "SELECT m FROM MiEmpresa m WHERE m.nombreEmpresa = :nombreEmpresa"),
    @NamedQuery(name = "MiEmpresa.findByDireccion", query = "SELECT m FROM MiEmpresa m WHERE m.direccion = :direccion"),
    @NamedQuery(name = "MiEmpresa.findByTelefono", query = "SELECT m FROM MiEmpresa m WHERE m.telefono = :telefono"),
    @NamedQuery(name = "MiEmpresa.findByRepresentanteLegal", query = "SELECT m FROM MiEmpresa m WHERE m.representanteLegal = :representanteLegal")})
public class MiEmpresa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_mi_empresa")
    private Integer idMiEmpresa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NIT")
    private int nit;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nombre_empresa")
    private String nombreEmpresa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "direccion")
    private String direccion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "telefono")
    private String telefono;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "representante_legal")
    private String representanteLegal;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMiEmpresa")
    private List<Servicios> serviciosList;

    public MiEmpresa() {
    }

    public MiEmpresa(Integer idMiEmpresa) {
        this.idMiEmpresa = idMiEmpresa;
    }

    public MiEmpresa(Integer idMiEmpresa, int nit, String nombreEmpresa, String direccion, String telefono, String representanteLegal) {
        this.idMiEmpresa = idMiEmpresa;
        this.nit = nit;
        this.nombreEmpresa = nombreEmpresa;
        this.direccion = direccion;
        this.telefono = telefono;
        this.representanteLegal = representanteLegal;
    }

    public Integer getIdMiEmpresa() {
        return idMiEmpresa;
    }

    public void setIdMiEmpresa(Integer idMiEmpresa) {
        this.idMiEmpresa = idMiEmpresa;
    }

    public int getNit() {
        return nit;
    }

    public void setNit(int nit) {
        this.nit = nit;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getRepresentanteLegal() {
        return representanteLegal;
    }

    public void setRepresentanteLegal(String representanteLegal) {
        this.representanteLegal = representanteLegal;
    }

    @XmlTransient
    public List<Servicios> getServiciosList() {
        return serviciosList;
    }

    public void setServiciosList(List<Servicios> serviciosList) {
        this.serviciosList = serviciosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMiEmpresa != null ? idMiEmpresa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MiEmpresa)) {
            return false;
        }
        MiEmpresa other = (MiEmpresa) object;
        if ((this.idMiEmpresa == null && other.idMiEmpresa != null) || (this.idMiEmpresa != null && !this.idMiEmpresa.equals(other.idMiEmpresa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.adsicbi.jardinapp.jpa.entities.MiEmpresa[ idMiEmpresa=" + idMiEmpresa + " ]";
    }
    
}
