/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Paola Andrea
 */
@Entity
@Table(name = "encuestas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Encuestas.findAll", query = "SELECT e FROM Encuestas e"),
    @NamedQuery(name = "Encuestas.findByIdEncuesta", query = "SELECT e FROM Encuestas e WHERE e.idEncuesta = :idEncuesta"),
    @NamedQuery(name = "Encuestas.findByDato", query = "SELECT e FROM Encuestas e WHERE e.dato = :dato"),
    @NamedQuery(name = "Encuestas.findByNumeroHijos", query = "SELECT e FROM Encuestas e WHERE e.numeroHijos = :numeroHijos"),
    @NamedQuery(name = "Encuestas.findByRangoEdades", query = "SELECT e FROM Encuestas e WHERE e.rangoEdades = :rangoEdades")})
public class Encuestas implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_encuesta")
    private Integer idEncuesta;
    @Column(name = "dato")
    private Boolean dato;
    @Column(name = "numero_hijos")
    private Integer numeroHijos;
    @Column(name = "rango_edades")
    private Integer rangoEdades;
    @OneToMany(mappedBy = "idEncuesta")
    private List<Usuarios> usuariosList;

    public Encuestas() {
    }

    public Encuestas(Integer idEncuesta) {
        this.idEncuesta = idEncuesta;
    }

    public Integer getIdEncuesta() {
        return idEncuesta;
    }

    public void setIdEncuesta(Integer idEncuesta) {
        this.idEncuesta = idEncuesta;
    }

    public Boolean getDato() {
        return dato;
    }

    public void setDato(Boolean dato) {
        this.dato = dato;
    }

    public Integer getNumeroHijos() {
        return numeroHijos;
    }

    public void setNumeroHijos(Integer numeroHijos) {
        this.numeroHijos = numeroHijos;
    }

    public Integer getRangoEdades() {
        return rangoEdades;
    }

    public void setRangoEdades(Integer rangoEdades) {
        this.rangoEdades = rangoEdades;
    }

    @XmlTransient
    public List<Usuarios> getUsuariosList() {
        return usuariosList;
    }

    public void setUsuariosList(List<Usuarios> usuariosList) {
        this.usuariosList = usuariosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEncuesta != null ? idEncuesta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Encuestas)) {
            return false;
        }
        Encuestas other = (Encuestas) object;
        if ((this.idEncuesta == null && other.idEncuesta != null) || (this.idEncuesta != null && !this.idEncuesta.equals(other.idEncuesta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.adsicbi.jardinapp.jpa.entities.Encuestas[ idEncuesta=" + idEncuesta + " ]";
    }
    
}
