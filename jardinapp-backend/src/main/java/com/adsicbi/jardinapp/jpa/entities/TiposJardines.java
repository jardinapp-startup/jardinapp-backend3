/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.jpa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author andres
 */
@Entity
@Table(name = "tipos_jardines")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TiposJardines.findAll", query = "SELECT t FROM TiposJardines t"),
    @NamedQuery(name = "TiposJardines.findByIdTipoJardines", query = "SELECT t FROM TiposJardines t WHERE t.idTipoJardines = :idTipoJardines"),
    @NamedQuery(name = "TiposJardines.findByNombreTipoJardines", query = "SELECT t FROM TiposJardines t WHERE t.nombreTipoJardines LIKE :nombreTipoJardines")})
public class TiposJardines implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_tipo_jardines")
    private Integer idTipoJardines;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "nombre_tipo_jardines")
    private String nombreTipoJardines;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoJardines")
    private List<JardinesInfantiles> jardinesInfantilesList;

    public TiposJardines() {
    }

    public TiposJardines(Integer idTipoJardines) {
        this.idTipoJardines = idTipoJardines;
    }

    public TiposJardines(Integer idTipoJardines, String nombreTipoJardines) {
        this.idTipoJardines = idTipoJardines;
        this.nombreTipoJardines = nombreTipoJardines;
    }

    public Integer getIdTipoJardines() {
        return idTipoJardines;
    }

    public void setIdTipoJardines(Integer idTipoJardines) {
        this.idTipoJardines = idTipoJardines;
    }

    public String getNombreTipoJardines() {
        return nombreTipoJardines;
    }

    public void setNombreTipoJardines(String nombreTipoJardines) {
        this.nombreTipoJardines = nombreTipoJardines;
    }

    @XmlTransient
    public List<JardinesInfantiles> getJardinesInfantilesList() {
        return jardinesInfantilesList;
    }

    public void setJardinesInfantilesList(List<JardinesInfantiles> jardinesInfantilesList) {
        this.jardinesInfantilesList = jardinesInfantilesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoJardines != null ? idTipoJardines.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TiposJardines)) {
            return false;
        }
        TiposJardines other = (TiposJardines) object;
        if ((this.idTipoJardines == null && other.idTipoJardines != null) || (this.idTipoJardines != null && !this.idTipoJardines.equals(other.idTipoJardines))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.adsicbi.jardinapp.jpa.entities.TiposJardines[ idTipoJardines=" + idTipoJardines + " ]";
    }
    
}
