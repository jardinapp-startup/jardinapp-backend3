/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.rest.services;

import com.adsicbi.jardinapp.jpa.entities.Paises;
import com.adsicbi.jardinapp.jpa.sessions.PaisesFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author vbn
 */
@Path("paises")
public class PaisesRest {
    
    @EJB
    private PaisesFacade ejbPaisesFacade;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Paises> findAll() {
        return ejbPaisesFacade.findAll();
    }
    
    @GET
    @Path("nombre/{nombre}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Paises> findByNombre(@PathParam("nombre") String nombre) {
        return ejbPaisesFacade.findByNombre(nombre);
    }
}
