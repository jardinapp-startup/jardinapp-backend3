/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.rest.services;

import com.adsicbi.jardinapp.jpa.entities.Pedidos;
import com.adsicbi.jardinapp.jpa.entities.RespuestasInquietudes;
import com.adsicbi.jardinapp.jpa.entities.Usuarios;
import com.adsicbi.jardinapp.jpa.sessions.RespuestasInquietudesFacade;
import com.adsicbi.jardinapp.rest.auth.AuthUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nimbusds.jose.JOSEException;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author bratc
 */
@Stateless
@Path("respuestasinquietudes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RespuestasInquietudesRest {

     @EJB
    private RespuestasInquietudesFacade ejbRespuestasInquietudesFacade;
    
    @Context
    private HttpServletRequest request;
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(RespuestasInquietudes respuestasInquietudes) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        
           respuestasInquietudes.setFecha("");
            ejbRespuestasInquietudesFacade.create(respuestasInquietudes);
            return Response.ok().entity(gson.toJson(respuestasInquietudes)).build();
         
    }
    
    @GET
    @Path("byInquietudesId/{id}")
 
    public List<RespuestasInquietudes> findById(@PathParam("id") Integer id) {
        return ejbRespuestasInquietudesFacade.byInquietudesId(id);
    }
    
    @GET
   
    public List<RespuestasInquietudes> findAll() {
        return ejbRespuestasInquietudesFacade.findAll();
    }
}
