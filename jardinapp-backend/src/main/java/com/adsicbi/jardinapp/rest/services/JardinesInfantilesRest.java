/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.rest.services;

import com.adsicbi.jardinapp.jpa.entities.Hijos;
import com.adsicbi.jardinapp.jpa.entities.JardinesInfantiles;
import com.adsicbi.jardinapp.jpa.entities.JardinesInfantilesHasUsuarios;
import com.adsicbi.jardinapp.jpa.entities.Usuarios;
import com.adsicbi.jardinapp.jpa.sessions.HijosFacade;
import com.adsicbi.jardinapp.jpa.sessions.JardinesInfantilesFacade;
import com.adsicbi.jardinapp.jpa.sessions.JardinesInfantilesHasUsuariosFacade;
import com.adsicbi.jardinapp.jpa.sessions.UsuariosFacade;
import com.adsicbi.jardinapp.rest.auth.AuthUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.nimbusds.jose.JOSEException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author vbn
 */
@Path("jardinesinfantiles")
@Produces(MediaType.APPLICATION_JSON)
public class JardinesInfantilesRest {
    
    @EJB
    private UsuariosFacade ejbUsuariosFacade;
    
    @EJB
    private JardinesInfantilesFacade ejbJardinesInfantilesFacade;
    @EJB
    private HijosFacade ejbHijosFacade;
    
    @EJB
    private JardinesInfantilesHasUsuariosFacade ejbJardinesInfantilesHasUsuariosFacade;
    
     @Context
    private HttpServletRequest request;
     
     //crea el jardin  y tambien crea en la tabla de muchos a muchos JardinesInfantilesHasUsuarios el registro del usuario que administra el jardin
    @POST
    @RolesAllowed({"adminJ"})
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(JardinesInfantiles jardindesInfantiles) throws ParseException, JOSEException {
 
    JardinesInfantiles jardinInfantil = ejbJardinesInfantilesFacade.createJardin(jardindesInfantiles); 
    JardinesInfantilesHasUsuarios jardinesHasUsuarios = new JardinesInfantilesHasUsuarios();
            jardinesHasUsuarios.setIdJardinInfantil(jardinInfantil);      
            jardinesHasUsuarios.setIdUsuarios(new Usuarios(
                    Integer.parseInt(
                            AuthUtils.getSubject(
                            request.getHeader(AuthUtils.AUTH_HEADER_KEY)))));
            
    ejbJardinesInfantilesHasUsuariosFacade.create(jardinesHasUsuarios);
  
    }
    
    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") Integer id, JardinesInfantiles jardindesInfantiles) {
        
         ejbJardinesInfantilesFacade.edit(jardindesInfantiles);
    }
    
    @PUT
    @RolesAllowed({"adminJ"})
    @Path("inhabilit/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response inhabilit(@PathParam("id") Integer id) {
       JardinesInfantiles jardin=ejbJardinesInfantilesFacade.find(id);
        System.out.println("------");
        System.out.println(jardin.getEstado());
        if(jardin.getEstado()) {
            jardin.setEstado(Boolean.FALSE);
        } else {
            jardin.setEstado(Boolean.TRUE);
        }
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        
        try{
            ejbJardinesInfantilesFacade.edit(jardin);
            return Response.ok()
                    .entity(gson.toJson("El estado de Jardín se cambió satisfactoriamente"))
                    .build();
        } catch(Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,null,ex);
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson("Error de persistencia")).build();
        }
    }
    
    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
       
        
        ejbJardinesInfantilesFacade.remove(ejbJardinesInfantilesFacade.find(id));
    }
    
    @GET
    @Path("public")
    @RolesAllowed({"adminP","adminJ","usuario","empresario"})
    @Produces(MediaType.APPLICATION_JSON)
    public List<JardinesInfantiles> findAllPublic() {
        return ejbJardinesInfantilesFacade.findAllPublic();
    } 
    
    @GET
    @RolesAllowed({"adminP"})
    @Produces(MediaType.APPLICATION_JSON)
    public List<JardinesInfantiles> findAllManager() {
        return ejbJardinesInfantilesFacade.findAll();
    }
    
    @GET
    @Path("mijardin")
    @RolesAllowed({"adminJ"})
    @Produces(MediaType.APPLICATION_JSON)
    public List<JardinesInfantiles> findByAdminJardin() throws ParseException, JOSEException {
        Integer id= Integer.parseInt(
                            AuthUtils.getSubject(
                            request.getHeader(AuthUtils.AUTH_HEADER_KEY)));
        
        return  ejbJardinesInfantilesFacade.findByAdminJardin(id);
        
       // return null;
    }
    
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public JardinesInfantiles findById(@PathParam("id") Integer id) {
        return ejbJardinesInfantilesFacade.find(id);
    }
    // LIKE JARDINRES
    @GET
    @Path("search/{data}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<JardinesInfantiles> findJardin(@PathParam("data") String data) {
        return ejbJardinesInfantilesFacade.findJardin(data);
    }
    // LIKE JARDINRES
    /*
    @GET
    @Path("mis")
    @Produces(MediaType.APPLICATION_JSON)
    public void findMisClientes(){
        
       
       
    }
    */
    @GET
    @Path("bycliente/{idJardin}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Hijos> findByClientes(@PathParam("idJardin") Integer idJardin){
      
       return ejbJardinesInfantilesFacade.findByClientes(idJardin); 
     
      
        
      
    }
    
    @GET
    @Path("findUsuarioByEmail/{email}")
    public Response findUsuarioByEmail(@PathParam("email") String email) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        if (ejbJardinesInfantilesFacade.findByEmail(email) != null) {
            return Response
                    .status(Response.Status.CONFLICT)
                    .entity(gson.toJson("false"))
                    .build();
        } else {

            return Response
                    .status(Response.Status.OK)
                    .entity(gson.toJson("true"))
                    .build();
        }
    }
}
