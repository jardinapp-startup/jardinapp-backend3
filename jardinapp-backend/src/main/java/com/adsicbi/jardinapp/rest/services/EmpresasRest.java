/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.rest.services;

import com.adsicbi.jardinapp.jpa.entities.Empresas;
import com.adsicbi.jardinapp.jpa.entities.Usuarios;
import com.adsicbi.jardinapp.jpa.sessions.EmpresasFacade;
import com.adsicbi.jardinapp.rest.auth.AuthUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nimbusds.jose.JOSEException;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author bravo
 */
@Path("empresas")
@Produces(MediaType.APPLICATION_JSON)

public class EmpresasRest {
    

    @EJB
    private EmpresasFacade ejbEmpresasFacade;
    
    @Context
    private HttpServletRequest request;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(Empresas empresas) throws JOSEException {
        GsonBuilder gsonBuilder=new GsonBuilder();
        Gson gson=gsonBuilder.create();
        try{
            empresas.setIdUsuarios(
                       new Usuarios(
                              Integer.parseInt(
                            AuthUtils.getSubject(
                            request.getHeader(AuthUtils.AUTH_HEADER_KEY)))));
             ejbEmpresasFacade.create(empresas);
             return Response.ok().entity(gson.toJson("Se creo la empresa satisfactoriamente")).build();
        }catch(ParseException | JOSEException | NumberFormatException ex){
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,null,ex);
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson("Error de persistencia")).build();
         }
        
    }

 
    @PUT
    @Path("{id}")

    public void edit(@PathParam("id") Integer id, Empresas empresas) {
        ejbEmpresasFacade.edit(empresas);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        ejbEmpresasFacade.remove(ejbEmpresasFacade.find(id));
    }

    @GET
    public List<Empresas> findAll() {
        return ejbEmpresasFacade.findAll();
    }

    @GET
    @Path("{id}")
    public Empresas findById(@PathParam("id") Integer id) {
        return ejbEmpresasFacade.find(id);
    }
    
    @PUT
    @Path("inhabilit/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response inhabilit(@PathParam("id") Integer id) {
        Empresas empresa = ejbEmpresasFacade.find(id);

        if (empresa.getEstado()) {
            empresa.setEstado(Boolean.FALSE);
        } else {
            empresa.setEstado(Boolean.TRUE);
        }
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        try {
            ejbEmpresasFacade.edit(empresa);
            return Response.ok()
                    .entity(gson.toJson("El estado de la empresa se cambió satisfactoriamente"))
                    .build();
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson("Error de persistencia")).build();
        }
    }
}

