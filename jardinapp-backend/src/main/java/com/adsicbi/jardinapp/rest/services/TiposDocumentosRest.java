/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.rest.services;

import com.adsicbi.jardinapp.jpa.entities.TiposDocumentos;
import com.adsicbi.jardinapp.jpa.sessions.TiposDocumentosFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author adsi3
 */
@Path("tiposdocumentos")
public class TiposDocumentosRest {
    
    @EJB
    private TiposDocumentosFacade ejbTiposDocumentosFacade;  
    
      @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<TiposDocumentos> findAll() {
        return ejbTiposDocumentosFacade.findAll();
    }  
   
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public TiposDocumentos findById(@PathParam("id") String id) {
        return ejbTiposDocumentosFacade.find(id);
    }
    
    @GET
    @Path("nombre/{query}")
    public List<TiposDocumentos> findByNombre(@PathParam("query") String query){
        return ejbTiposDocumentosFacade.findByNombre(query);
    } 
    
}
