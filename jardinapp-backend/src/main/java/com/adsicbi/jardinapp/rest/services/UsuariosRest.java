package com.adsicbi.jardinapp.rest.services;

import com.adsicbi.jardinapp.jpa.entities.Roles;
import com.adsicbi.jardinapp.jpa.entities.Usuarios;
import com.adsicbi.jardinapp.jpa.sessions.UsuariosFacade;
import com.adsicbi.jardinapp.rest.auth.DigestUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import net.minidev.json.JSONObject;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;

import org.apache.commons.mail.HtmlEmail;

/**
 *
 * @author leoandresm
 */
@Path("usuarios")
@Produces(MediaType.APPLICATION_JSON)
public class UsuariosRest {

    @EJB
    private UsuariosFacade ejbUsuariosFacade;
   

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(Usuarios usuario) throws EmailException {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        if (ejbUsuariosFacade.findByEmail(usuario.getEmail()) == null) {
            usuario.setRolesList(new ArrayList<Roles>());
            usuario.getRolesList().add(new Roles("usuario"));
            //usuario.get().add(new Usuarios())
            //***************
            String htmlEmail = "<!DOCTYPE html>\n"
                    + "<html lang=\"es\">\n"
                    + "<head>\n"
                    + "  <meta charset=\"UTF-8\">\n"
                    + "  <title>Bienvenido a JardinApp</title>\n"
                    + "  <style>\n"
                    + "      @font-face {\n"
                    + "        font-family: MyriadWebPro;\n"
                    + "        src: url('fonts/MyriadWebPro.ttf');\n"
                    + "      }\n"
                    + "  </style>\n"
                    + "</head>\n"
                    + "<body style=\"\n"
                    + "       margin:0;\n"
                    + "       padding:0;\n"
                    + "       font-family: MyriadWebPro;\n"
                    + "       background-color:#f1f1f1;\">\n"
                    + "\n"
                    + "  <figure style=\"\n"
                    + "    display: block;\n"
                    + "    background: #0288D1;\n"
                    + "    padding:1em;\n"
                    + "    margin: 0 auto;\n"
                    + "    text-align: center;\n"
                    + "    max-width: 95%;\">\n"
                    + "    <img src=\"http://104.131.38.243:8181/html/logo.png\" style=\"width: 9em;\"/>\n"
                    + "    <figcaption style=\"color:#fff; margin:0;\">Asegura el Futuro de tus hijos.</figcaption> \n"
                    + "  </figure>\n"
                    + "\n"
                    + "  <section style=\"\n"
                    + "    text-align: center;\n"
                    + "    margin: 0em auto;\n"
                    + "    max-width: 95%;\n"
                    + "    padding: 1em;\n"
                    + "    background-color: #fff;\">\n"
                    + "\n"
                    + "    <div>\n"
                    + "      <h2 style=\"color:#0288D1; text-shadow: 2px 2px 5px rgb(195, 195, 195);\">Bienvenido " + usuario.getNombres() + " " + usuario.getApellidos() + "</h2>\n"
                    + "      <p style=\"max-width: 90%; margin: 0 auto;\">\n"
                    + "        Te damos la más calurosa bienvenida a JardinApp, de ahora en adelante podras disfrutar de nuestros servicios accediendo a la plataforma mediante tu correo y contraseña.\n"
                    + "      </p>\n"
                    + "\n"
                    + "      <h4 style=\"text-align: left; margin:1em 0 0 0; padding-left: 1.5em; color:#0288D1; text-shadow: 2px 2px 5px rgb(195, 195, 195);\">Estos son tus datos:</h4>\n"
                    + "      <ul style=\"list-style: none; text-align: left; margin: 0;\">\n"
                    + "        <li>Correo: " + usuario.getEmail() + "</li>\n"
                    + "        <li>Contraseña:  " + usuario.getPassword() + "</li>\n"
                    + "      </ul>\n"
                    + "      <a href=\"jardinappfront-bratc.rhcloud.com\" style=\"text-decoration: none; background:#0288D1; color:#fff; padding: .5em;\n"
                    + "    border-radius: 7px;\n"
                    + "    display: block;\n"
                    + "    max-width: 5em;\n"
                    + "    margin: 1em auto;\n"
                    + "    text-shadow: 2px 2px 2px #515151;\n"
                    + "    box-shadow: 2px 2px 4px 0 #515151;\">Ingresar</a>\n"
                    + "    </article>\n"
                    + "    <article style=\"\n"
                    + "        width: 95%;\n"
                    + "        padding: 1em 0;\n"
                    + "        margin: 1em auto;\n"
                    + "        border-top: 1px solid #0288D1;\">\n"
                    + "        <h3 style=\"margin: 0; color:#0288D1;\">JardinApp</h3>\n"
                    + "        <p style=\"margin: 0;\">Palmira</p>\n"
                    + "        <p style=\"\n"
                    + "          margin: 0;\n"
                    + "          text-transform: uppercase;\">2016</p>\n"
                    + "    </div>\n"
                    + "</section>\n"
                    + "</body>\n"
                    + "</html>";
            try {
                usuario.setPassword(DigestUtil.generateDigest(usuario.getPassword()));
            } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
                Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            }
            //***************
            ejbUsuariosFacade.create(usuario);
            //***EMAIL

            try {
                ConfigEmailSmtp configEmail = new ConfigEmailSmtp();
                HtmlEmail email = new HtmlEmail();
                email.setHostName(configEmail.getHostName());
                email.setSmtpPort(465);
                email.setAuthenticator(new DefaultAuthenticator(configEmail.getUsername(), configEmail.getPassword()));
                email.setSSL(true);
                email.setFrom(configEmail.getEmailFrom());
                email.setSubject("Bienvenido a JardinApp");
                email.setHtmlMsg(htmlEmail);

                email.addTo(usuario.getEmail());
                email.send();
            } catch (Exception ex) {
                System.out.println("Error " + ex);
            }

            //***EMAIL
            return Response.ok()
                    .entity(gson.toJson("El usuario fue creado exitosamente"))
                    .build();
        } else {
            return Response
                    .status(Response.Status.CONFLICT)
                    .entity(gson.toJson("El email ya esta registrado"))
                    .build();
        }

    }

    @POST
    @Path("createjardin")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createJardin(Usuarios usuario) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        if (ejbUsuariosFacade.findByEmail(usuario.getEmail()) == null) {
            usuario.setRolesList(new ArrayList<Roles>());
            usuario.getRolesList().add(new Roles("adminJ"));
            //***************
            String htmlEmail = "<!DOCTYPE html>\n"
                    + "<html lang=\"es\">\n"
                    + "<head>\n"
                    + "  <meta charset=\"UTF-8\">\n"
                    + "  <title>Bienvenido a JardinApp</title>\n"
                    + "  <style>\n"
                    + "      @font-face {\n"
                    + "        font-family: MyriadWebPro;\n"
                    + "        src: url('fonts/MyriadWebPro.ttf');\n"
                    + "      }\n"
                    + "  </style>\n"
                    + "</head>\n"
                    + "<body style=\"\n"
                    + "       margin:0;\n"
                    + "       padding:0;\n"
                    + "       font-family: MyriadWebPro;\n"
                    + "       background-color:#f1f1f1;\">\n"
                    + "\n"
                    + "  <figure style=\"\n"
                    + "    display: block;\n"
                    + "    background: #0288D1;\n"
                    + "    padding:1em;\n"
                    + "    margin: 0 auto;\n"
                    + "    text-align: center;\n"
                    + "    max-width: 95%;\">\n"
                    + "    <img src=\"http://104.131.38.243:8181/html/logo.png\" style=\"width: 9em;\"/>\n"
                    + "    <figcaption style=\"color:#fff; margin:0;\">Asegura el Futuro de tus hijos.</figcaption> \n"
                    + "  </figure>\n"
                    + "\n"
                    + "  <section style=\"\n"
                    + "    text-align: center;\n"
                    + "    margin: 0em auto;\n"
                    + "    max-width: 95%;\n"
                    + "    padding: 1em;\n"
                    + "    background-color: #fff;\">\n"
                    + "\n"
                    + "    <div>\n"
                    + "      <h2 style=\"color:#0288D1; text-shadow: 2px 2px 5px rgb(195, 195, 195);\">Bienvenido " + usuario.getNombres() + " " + usuario.getApellidos() + "</h2>\n"
                    + "      <p style=\"max-width: 90%; margin: 0 auto;\">\n"
                    + "        Te damos la más calurosa bienvenida a JardinApp, de ahora en adelante podras disfrutar de nuestros servicios accediendo a la plataforma mediante tu correo y contraseña.\n"
                    + "      </p>\n"
                    + "\n"
                    + "      <h4 style=\"text-align: left; margin:1em 0 0 0; padding-left: 1.5em; color:#0288D1; text-shadow: 2px 2px 5px rgb(195, 195, 195);\">Estos son tus datos:</h4>\n"
                    + "      <ul style=\"list-style: none; text-align: left; margin: 0;\">\n"
                    + "        <li>Correo: " + usuario.getEmail() + "</li>\n"
                    + "        <li>Contraseña:  " + usuario.getPassword() + "</li>\n"
                    + "      </ul>\n"
                    + "      <a href=\"jardinappfront-bratc.rhcloud.com\" style=\"text-decoration: none; background:#0288D1; color:#fff; padding: .5em;\n"
                    + "    border-radius: 7px;\n"
                    + "    display: block;\n"
                    + "    max-width: 5em;\n"
                    + "    margin: 1em auto;\n"
                    + "    text-shadow: 2px 2px 2px #515151;\n"
                    + "    box-shadow: 2px 2px 4px 0 #515151;\">Ingresar</a>\n"
                    + "    </article>\n"
                    + "    <article style=\"\n"
                    + "        width: 95%;\n"
                    + "        padding: 1em 0;\n"
                    + "        margin: 1em auto;\n"
                    + "        border-top: 1px solid #0288D1;\">\n"
                    + "        <h3 style=\"margin: 0; color:#0288D1;\">JardinApp</h3>\n"
                    + "        <p style=\"margin: 0;\">Palmira</p>\n"
                    + "        <p style=\"\n"
                    + "          margin: 0;\n"
                    + "          text-transform: uppercase;\">2016</p>\n"
                    + "    </div>\n"
                    + "</section>\n"
                    + "</body>\n"
                    + "</html>";
            try {
                usuario.setPassword(DigestUtil.generateDigest(usuario.getPassword()));
            } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
                Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            }
            //***************
            ejbUsuariosFacade.create(usuario);
            try {
                ConfigEmailSmtp configEmail = new ConfigEmailSmtp();
                HtmlEmail email = new HtmlEmail();
                email.setHostName(configEmail.getHostName());
                email.setSmtpPort(465);
                email.setAuthenticator(new DefaultAuthenticator(configEmail.getUsername(), configEmail.getPassword()));
                email.setSSL(true);
                email.setFrom(configEmail.getEmailFrom());
                email.setSubject("Bienvenido a JardinApp");
                email.setHtmlMsg(htmlEmail);

                email.addTo(usuario.getEmail());
                email.send();
            } catch (Exception ex) {
                System.out.println("Error " + ex);
            }
            return Response.ok()
                    .entity(gson.toJson("El jardin fue creado exitosamente"))
                    .build();
        } else {
            return Response
                    .status(Response.Status.CONFLICT)
                    .entity(gson.toJson("El email ya esta registrado"))
                    .build();
        }

    }

    @POST
    @Path("createempresario")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createEmpresario(Usuarios usuario) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        if (ejbUsuariosFacade.findByEmail(usuario.getEmail()) == null) {
            usuario.setRolesList(new ArrayList<Roles>());
            usuario.getRolesList().add(new Roles("empresario"));
            //***************
            String htmlEmail = "<!DOCTYPE html>\n"
                    + "<html lang=\"es\">\n"
                    + "<head>\n"
                    + "  <meta charset=\"UTF-8\">\n"
                    + "  <title>Bienvenido a JardinApp</title>\n"
                    + "  <style>\n"
                    + "      @font-face {\n"
                    + "        font-family: MyriadWebPro;\n"
                    + "        src: url('fonts/MyriadWebPro.ttf');\n"
                    + "      }\n"
                    + "  </style>\n"
                    + "</head>\n"
                    + "<body style=\"\n"
                    + "       margin:0;\n"
                    + "       padding:0;\n"
                    + "       font-family: MyriadWebPro;\n"
                    + "       background-color:#f1f1f1;\">\n"
                    + "\n"
                    + "  <figure style=\"\n"
                    + "    display: block;\n"
                    + "    background: #0288D1;\n"
                    + "    padding:1em;\n"
                    + "    margin: 0 auto;\n"
                    + "    text-align: center;\n"
                    + "    max-width: 95%;\">\n"
                    + "    <img src=\"http://104.131.38.243:8181/html/logo.png\" style=\"width: 9em;\"/>\n"
                    + "    <figcaption style=\"color:#fff; margin:0;\">Asegura el Futuro de tus hijos.</figcaption> \n"
                    + "  </figure>\n"
                    + "\n"
                    + "  <section style=\"\n"
                    + "    text-align: center;\n"
                    + "    margin: 0em auto;\n"
                    + "    max-width: 95%;\n"
                    + "    padding: 1em;\n"
                    + "    background-color: #fff;\">\n"
                    + "\n"
                    + "    <div>\n"
                    + "      <h2 style=\"color:#0288D1; text-shadow: 2px 2px 5px rgb(195, 195, 195);\">Bienvenido " + usuario.getNombres() + " " + usuario.getApellidos() + "</h2>\n"
                    + "      <p style=\"max-width: 90%; margin: 0 auto;\">\n"
                    + "        Te damos la más calurosa bienvenida a JardinApp, de ahora en adelante podras disfrutar de nuestros servicios accediendo a la plataforma mediante tu correo y contraseña.\n"
                    + "      </p>\n"
                    + "\n"
                    + "      <h4 style=\"text-align: left; margin:1em 0 0 0; padding-left: 1.5em; color:#0288D1; text-shadow: 2px 2px 5px rgb(195, 195, 195);\">Estos son tus datos:</h4>\n"
                    + "      <ul style=\"list-style: none; text-align: left; margin: 0;\">\n"
                    + "        <li>Correo: " + usuario.getEmail() + "</li>\n"
                    + "        <li>Contraseña:  " + usuario.getPassword() + "</li>\n"
                    + "      </ul>\n"
                    + "      <a href=\"jardinappfront-bratc.rhcloud.com\" style=\"text-decoration: none; background:#0288D1; color:#fff; padding: .5em;\n"
                    + "    border-radius: 7px;\n"
                    + "    display: block;\n"
                    + "    max-width: 5em;\n"
                    + "    margin: 1em auto;\n"
                    + "    text-shadow: 2px 2px 2px #515151;\n"
                    + "    box-shadow: 2px 2px 4px 0 #515151;\">Ingresar</a>\n"
                    + "    </article>\n"
                    + "    <article style=\"\n"
                    + "        width: 95%;\n"
                    + "        padding: 1em 0;\n"
                    + "        margin: 1em auto;\n"
                    + "        border-top: 1px solid #0288D1;\">\n"
                    + "        <h3 style=\"margin: 0; color:#0288D1;\">JardinApp</h3>\n"
                    + "        <p style=\"margin: 0;\">Palmira</p>\n"
                    + "        <p style=\"\n"
                    + "          margin: 0;\n"
                    + "          text-transform: uppercase;\">2016</p>\n"
                    + "    </div>\n"
                    + "</section>\n"
                    + "</body>\n"
                    + "</html>";
            try {
                usuario.setPassword(DigestUtil.generateDigest(usuario.getPassword()));
            } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
                Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            }
            //***************
            ejbUsuariosFacade.create(usuario);
            try {
                ConfigEmailSmtp configEmail = new ConfigEmailSmtp();
                HtmlEmail email = new HtmlEmail();
                email.setHostName(configEmail.getHostName());
                email.setSmtpPort(465);
                email.setAuthenticator(new DefaultAuthenticator(configEmail.getUsername(), configEmail.getPassword()));
                email.setSSL(true);
                email.setFrom(configEmail.getEmailFrom());
                email.setSubject("Bienvenido a JardinApp");
                email.setHtmlMsg(htmlEmail);

                email.addTo(usuario.getEmail());
                email.send();
            } catch (Exception ex) {
                System.out.println("Error " + ex);
            }
            return Response.ok()
                    .entity(gson.toJson("El jardin fue creado exitosamente"))
                    .build();
        } else {
            return Response
                    .status(Response.Status.CONFLICT)
                    .entity(gson.toJson("El email ya esta registrado"))
                    .build();
        }

    }

    @PUT
    @RolesAllowed({"adminP", "adminJ"})
    @Path("{id}")
    public Response edit(@PathParam("id") Integer id, Usuarios usuario) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        try {
    
            ejbUsuariosFacade.edit(usuario);
            return Response.ok()
                    .entity(gson.toJson("El usuario se actualizo correctamente"))
                    .build();

        } catch (EJBException ex) {
            String msg = "";
            Throwable cause = ex.getCause();
            if (cause != null) {
                if (cause instanceof ConstraintViolationException) {
                    ConstraintViolationException constraintViolationException = (ConstraintViolationException) cause;
                    for (ConstraintViolation<?> constraintViolation : constraintViolationException.getConstraintViolations()) {
                        msg += "{";
                        msg += "entity: " + constraintViolation.getLeafBean().toString() + ",";
                        msg += "field: " + constraintViolation.getPropertyPath().toString() + ",";
                        msg += "invalidValue: " + constraintViolation.getInvalidValue().toString() + ",";
                        msg += "error: " + constraintViolation.getMessage();
                        msg += "}";
                    }
                } else {
                    msg = cause.getLocalizedMessage();
                }
            }
            if (msg.length() > 0) {
                return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson(msg)).build();
            } else {
                return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson("Error de persistencia")).build();
            }
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson("Error de persistencia")).build();
        }
    }
// Update pass

    @PUT
    @Path("updatepass/{id}")
    public void updatePass(@PathParam("id") Integer id, Usuarios usuario) {
        String newPass = usuario.getPassword();
        usuario = ejbUsuariosFacade.find(id);
        try {

            usuario.setPassword(DigestUtil.generateDigest(newPass));
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
        }
        ejbUsuariosFacade.edit(usuario);
    }

    @PUT
    @Path("inhabilit/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response inhabilit(@PathParam("id") Integer id) {
        Usuarios usuario = ejbUsuariosFacade.find(id);

        if (usuario.getEstado()) {
            usuario.setEstado(Boolean.FALSE);
        } else {
            usuario.setEstado(Boolean.TRUE);
        }
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        try {
            ejbUsuariosFacade.edit(usuario);
            return Response.ok()
                    .entity(gson.toJson("El estado de Usuarios se cambió satisfactoriamente"))
                    .build();
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson("Error de persistencia")).build();
        }
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        ejbUsuariosFacade.remove(ejbUsuariosFacade.find(id));
    }

    @GET
   // @RolesAllowed({"adminP"})
    public List<Usuarios> findAll() {
        return ejbUsuariosFacade.findAll();
    }

    @GET
    @Path("usuario")
    public List<Usuarios> findAllU() {
        return ejbUsuariosFacade.findAllU();
    }

    @GET
    @Path("{id}")
    public Usuarios findById(@PathParam("id") Integer id) {
        return ejbUsuariosFacade.find(id);
    }

    @GET
    @Path("findUsuarioByDocumento/{documento}")
    public Response findUsuarioByDocumento(@PathParam("documento") String documento) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        if (ejbUsuariosFacade.findUsuarioByDocumento(documento) != null) {
            return Response 
                    .status(Response.Status.CONFLICT)
                    .entity(gson.toJson("false"))
                    .build();
        } else {

            return Response
                    .status(Response.Status.OK)
                    .entity(gson.toJson("true"))
                    .build();
        }
    }
    @GET
    @Path("findUsuarioByDocumentoHijos/{documento}")
    public Usuarios findUsuarioByDocumentoHijos(@PathParam("documento") String documento) {
       return ejbUsuariosFacade.findUsuarioByDocumento(documento);
    }

    @GET
    @Path("findUsuarioByEmail/{email}")
    public Response findUsuarioByEmail(@PathParam("email") String email) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        if (ejbUsuariosFacade.findByEmail(email) != null) {
            return Response
                    .status(Response.Status.CONFLICT)
                    .entity(gson.toJson("false"))
                    .build();
        } else {

            return Response
                    .status(Response.Status.OK)
                    .entity(gson.toJson("true"))
                    .build();
        }
    }

    @GET
    @Path("nombre/{nombre}")
    public List<Usuarios> findByNombre(@PathParam("nombre") String nombre) {
        return ejbUsuariosFacade.findByNombre(nombre);
    }
    
    //Actualizar contraseña
    @PUT
    @Path("updatepassw/{id}/{passOld}/{passNew}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updatepassword(@PathParam("id") Integer id, @PathParam("passOld") String passOld,
            @PathParam("passNew") String passNew) {
        
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        
        //Find User by Id            
        Usuarios user = ejbUsuariosFacade.find(id);
        
        System.out.println(compararPassword(user, passOld));
        
        if (compararPassword(user, passOld)) {
            System.out.println("HOLA, CONTRASEÑAS COINCIDEN");
            try {
                //Cifrar Password
                String nueva = DigestUtil.generateDigest(passNew);

                user.setPassword(nueva);
                
                //Edit User
                ejbUsuariosFacade.edit(user);
                return Response.ok()
                    .entity(gson.toJson("CONTRASEÑAS COINCIDEN, ACTUALIZADA CORRECTAMENTE"))
                    .build();
            } catch (Exception e) {
                Logger.getLogger(UsuariosRest.class.getName()).log(Level.SEVERE, null, e);
                return Response.status(Response.Status.BAD_REQUEST)
                        .entity(gson.toJson("CONTRASEÑAS NO COINCIDEN, ERROR AL ACTUALIZAR"))
                        .build();
            }

        } else {
            System.out.println("LAS CONTRASEÑAS NO COINCIDEN");
            return Response.status(Response.Status.BAD_REQUEST)
                        .entity(gson.toJson("CONTRASEÑAS NO COINCIDEN, ERROR AL ACTUALIZAR"))
                        .build();
        }
    }
    private Boolean compararPassword(Usuarios user, String passAntigua) {

        try {
            String antigua = DigestUtil.generateDigest(passAntigua);
 
            if (antigua.equals(user.getPassword())) {
                return true;
            } else {
                return false;
            }
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            Logger.getLogger(UsuariosRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    //Aqui terminar actualizar contraseña

    @POST
    @Path("contacto")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response contacto(Contacto contacto) {

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        String htmlContactoJardin = "<!DOCTYPE html>\n"
                + "<html lang=\"es\">\n"
                + "<head>\n"
                + "  <meta charset=\"UTF-8\">\n"
                + "  <title>Bienvenido a JardinApp</title>\n"
                + "  <style>\n"
                + "      @font-face {\n"
                + "        font-family: MyriadWebPro;\n"
                + "        src: url('fonts/MyriadWebPro.ttf');\n"
                + "      }\n"
                + "  </style>\n"
                + "</head>\n"
                + "<body style=\"\n"
                + "       margin:0;\n"
                + "       padding:0;\n"
                + "       font-family: MyriadWebPro;\n"
                + "       background-color:#f1f1f1;\">\n"
                + "\n"
                + "  <figure style=\"\n"
                + "    display: block;\n"
                + "    background: #0288D1;\n"
                + "    padding:1em;\n"
                + "    margin: 0 auto;\n"
                + "    text-align: center;\n"
                + "    max-width: 95%;\">\n"
                + "    <img src=\"http://104.131.38.243:8181/html/logo.png\" style=\"width: 9em;\"/>\n"
                + "    <figcaption style=\"color:#fff; margin:0;\">Asegura el Futuro de tus hijos.</figcaption> \n"
                + "  </figure>\n"
                + "\n"
                + "  <section style=\"\n"
                + "    text-align: center;\n"
                + "    margin: 0em auto;\n"
                + "    max-width: 95%;\n"
                + "    padding: 1em;\n"
                + "    background-color: #fff;\">\n"
                + "\n"
                + "    <div>\n"
                + "      <h2 style=\"color:#0288D1; text-shadow: 2px 2px 5px rgb(195, 195, 195);\">Contacto: " + contacto.getNombre() + "</h2>\n"
                + "\n"
                + "      <ul style=\"list-style: none; text-align: left; margin: 0;\">\n"
                + "        <li>Email : " + contacto.getEmail() + "</li> \n"
                + "        <li>Mensaje:  " + contacto.getMensaje() + "</li> \n"
                + "      </ul>\n"
                + "    </article>\n"
                + "    <article style=\"\n"
                + "        width: 95%;\n"
                + "        padding: 1em 0;\n"
                + "        margin: 1em auto;\n"
                + "        border-top: 1px solid #0288D1;\">\n"
                + "        <h3 style=\"margin: 0; color:#0288D1;\">JardinApp</h3>\n"
                + "        <p style=\"margin: 0;\">Palmira</p>\n"
                + "        <p style=\"\n"
                + "          margin: 0;\n"
                + "          text-transform: uppercase;\">2016</p>\n"
                + "    </div>\n"
                + "</section>\n"
                + "</body>\n"
                + "</html>";
        try {
            ConfigEmailSmtp configEmail = new ConfigEmailSmtp();

            HtmlEmail emailJar = new HtmlEmail();
            emailJar.setHostName(configEmail.getHostName());
            emailJar.setSmtpPort(465);
            emailJar.setAuthenticator(new DefaultAuthenticator(configEmail.getUsername(), configEmail.getPassword()));
            emailJar.setSSL(true);
            emailJar.setFrom(configEmail.getEmailFrom());
            emailJar.setSubject("Contacto");
            emailJar.setHtmlMsg(htmlContactoJardin);

            emailJar.addTo(configEmail.getUsername());
            emailJar.send();

            HtmlEmail email = new HtmlEmail();

            email.setHostName(configEmail.getHostName());
            email.setSmtpPort(465);
            email.setAuthenticator(new DefaultAuthenticator(configEmail.getUsername(), configEmail.getPassword()));
            email.setSSL(true);
            email.setFrom(configEmail.getEmailFrom());
            email.setSubject("Contacto");
            email.setHtmlMsg("<!DOCTYPE html>\n"
                    + "<html lang=\"es\">\n"
                    + "<head>\n"
                    + "  <meta charset=\"UTF-8\">\n"
                    + "  <title>Bienvenido a JardinApp</title>\n"
                    + "</head>\n"
                    + "<body style=\"\n"
                    + "       margin:0;\n"
                    + "       padding:0;\n"
                    + "       font-family: MyriadWebPro;\n"
                    + "       background-color:#f1f1f1;\">\n"
                    + "\n"
                    + "  <figure style=\"\n"
                    + "    display: block;\n"
                    + "    background: #0288D1;\n"
                    + "    padding:1em;\n"
                    + "    margin: 0 auto;\n"
                    + "    text-align: center;\n"
                    + "    max-width: 95%;\">\n"
                    + "    <img src=\"http://104.131.38.243:8181/html/logo.png\" style=\"width: 9em;\"/>\n"
                    + "    <figcaption style=\"color:#fff; margin:0;\">Asegura el Futuro de tus hijos.</figcaption> \n"
                    + "  </figure>\n"
                    + "\n"
                    + "  <section style=\"\n"
                    + "    text-align: center;\n"
                    + "    margin: 0em auto;\n"
                    + "    max-width: 95%;\n"
                    + "    padding: 1em;\n"
                    + "    background-color: #fff;\">\n"
                    + "\n"
                    + "    <div>\n"
                    + "      <h2 style=\"color:#0288D1; text-shadow: 2px 2px 5px rgb(195, 195, 195);\">Hola  " + contacto.getNombre() + "!</h2>\n"
                    + " <p>Gracias por contactárte con nosotros, pronto responderemos tu mensaje</p>"
                    + "    </article>\n"
                    + "    <article style=\"\n"
                    + "        width: 95%;\n"
                    + "        padding: 1em 0;\n"
                    + "        margin: 1em auto;\n"
                    + "        border-top: 1px solid #0288D1;\">\n"
                    + "        <h3 style=\"margin: 0; color:#0288D1;\">JardinApp</h3>\n"
                    + "        <p style=\"margin: 0;\">Palmira</p>\n"
                    + "        <p style=\"\n"
                    + "          margin: 0;\n"
                    + "          text-transform: uppercase;\">2016</p>\n"
                    + "    </div>\n"
                    + "</section>\n"
                    + "</body>\n"
                    + "</html>");
            email.addTo(contacto.getEmail());
            email.send();
        } catch (Exception ex) {
            System.out.println("Error " + ex);
        }

        return Response.ok()
                .entity(gson.toJson("Mensaje enviado"))
                .build();
    }

}

class Contacto {

    String nombre;
    String email;
    String mensaje;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

}
