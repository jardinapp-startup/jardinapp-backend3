/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.rest.services;

import com.adsicbi.jardinapp.jpa.entities.Anuncios;
import com.adsicbi.jardinapp.jpa.sessions.AnunciosFacade;
import com.adsicbi.jardinapp.rest.auth.AuthUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nimbusds.jose.JOSEException;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author PC 4
 */
@Path("anuncios")
@Produces(MediaType.APPLICATION_JSON)
public class AnunciosRest {
    
    @EJB
    private AnunciosFacade ejbanunciosfacade;
    
      @Context
    private HttpServletRequest request;
            
    @POST
    @Consumes(MediaType.APPLICATION_JSON) 
    public void create(Anuncios anuncios){
        ejbanunciosfacade.create(anuncios);
    }
    
    
    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") Integer id, Anuncios anuncios){
        ejbanunciosfacade.edit(anuncios);
    }
    
    @PUT
    @Path("inhabilit/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response inhabilit(@PathParam("id") Integer id) {
         Anuncios anuncio=ejbanunciosfacade.find(id);
         if(anuncio.getEstado()){
             anuncio.setEstado(Boolean.FALSE);
             
         }else{
             anuncio.setEstado(Boolean.TRUE);
         }
        
         GsonBuilder gsonBuilder = new GsonBuilder();
         Gson gson = gsonBuilder.create();   
         
         try{
             ejbanunciosfacade.edit(anuncio);
             return Response.ok()
                     .entity(gson.toJson("El estado del anuncio se cambio satisfactoriamente"))
                     .build();
         }catch(Exception ex){
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,null,ex);
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson("Error de persistencia")).build();
         }
                 
          
         
         
    }
    
    
    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id){
        ejbanunciosfacade.remove(ejbanunciosfacade.find(id));
    }
     
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Anuncios> findAll(){
        return ejbanunciosfacade.findAll();
    }
    /*
    * Este API REST es para que un usuario adminJ pueda consultar los anuncios del jardin el cual él administra
    */
    @GET
    @Path("listaAdminJardin")
    @RolesAllowed({"adminJ"}) 
    @Produces(MediaType.APPLICATION_JSON)
    public List<Anuncios> findByJardinAdmin() throws ParseException, JOSEException {
        Integer id= Integer.parseInt(
                            AuthUtils.getSubject(
                            request.getHeader(AuthUtils.AUTH_HEADER_KEY)));
        
        return  ejbanunciosfacade.findByJardinAdmin(id);
        
       // return null; 
    }
    
    @GET
    @Path("anuncio")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Anuncios> findAllA(){
        return ejbanunciosfacade.findAllA();
    } 
    
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Anuncios findById(@PathParam("id") Integer id){
        return ejbanunciosfacade.find(id);
    }
    
    /* Este RES es para que un usuario con rol usuario pueda visualizar solo los anuncios del jardin al cual tiene matriculado su hijo
    *
    */
    @GET
    @RolesAllowed({"usuario"}) 
    @Path("anunciosmijardin")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Anuncios> findAnunciosByClienteJardin() throws ParseException, JOSEException{
        Integer id= Integer.parseInt(
                            AuthUtils.getSubject(
                            request.getHeader(AuthUtils.AUTH_HEADER_KEY))); 
        return ejbanunciosfacade.findAnunciosByClienteJardin(id);
    }
            
}

