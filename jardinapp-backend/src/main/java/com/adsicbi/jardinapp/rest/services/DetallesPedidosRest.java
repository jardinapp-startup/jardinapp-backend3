/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.rest.services;

import com.adsicbi.jardinapp.jpa.entities.DetallesPedidos;
import com.adsicbi.jardinapp.jpa.sessions.DetallesPedidosFacade;
import com.adsicbi.jardinapp.rest.auth.AuthUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nimbusds.jose.JOSEException;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("detallespedidos")
public class DetallesPedidosRest {
    
    @EJB
    private DetallesPedidosFacade ejbDetallesPedidosFacade;
    
    @Context
    private HttpServletRequest request;
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(DetallesPedidos detallesPedidos) {
        ejbDetallesPedidosFacade.create(detallesPedidos);
    }
    
    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") Integer id, DetallesPedidos detallesPedidos) {
        ejbDetallesPedidosFacade.edit(detallesPedidos);
    }
    
    /*@DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        ejbDetallesPedidosFacade.remove(ejbDetallesPedidosFacade.find(id));
    }*/
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<DetallesPedidos> findAll() {
        return ejbDetallesPedidosFacade.findAll();
    }
    
    /*@GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public DetallesPedidos findById(@PathParam("id") Integer id) {
        return ejbDetallesPedidosFacade.find(id);
    }*/ 
    
    @GET
    @Path("byusuario")
    @Produces(MediaType.APPLICATION_JSON)
    public List<DetallesPedidos> findByIdUsuario(){
        try{
            return ejbDetallesPedidosFacade.findByIdUsuario(Integer.parseInt(
                    AuthUtils.getSubject(
                      request.getHeader(AuthUtils.AUTH_HEADER_KEY))));
        }catch (ParseException | JOSEException ex){
            return null;
        }
    }
}  

