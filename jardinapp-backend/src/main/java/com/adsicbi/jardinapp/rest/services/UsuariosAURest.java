/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.rest.services;


import com.adsicbi.jardinapp.jpa.entities.UsuariosAU;
import com.adsicbi.jardinapp.jpa.sessions.UsuariosAUFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author adsi3
 */
@Path("usuariosAU")
@Produces(MediaType.APPLICATION_JSON)
public class UsuariosAURest {
    
    @EJB
    private UsuariosAUFacade usuariosAUFacade;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UsuariosAU> findAll() {
        return usuariosAUFacade.findAll();
    }
    
}
