/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.rest.services;

import com.adsicbi.jardinapp.jpa.entities.Comentarios;
import com.adsicbi.jardinapp.jpa.entities.Usuarios;
import com.adsicbi.jardinapp.jpa.sessions.ComentariosFacade;
import com.adsicbi.jardinapp.jpa.sessions.JardinesInfantilesFacade;
import com.adsicbi.jardinapp.rest.auth.AuthUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nimbusds.jose.JOSEException;
import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author adsi3
 */
@Path("comentarios")
@Produces(MediaType.APPLICATION_JSON)
public class ComentariosRest {
    
    @EJB
    private ComentariosFacade ejbComentariosFacade;
    @EJB
    private JardinesInfantilesFacade ejbjardinesInfantilesFacade;
    
    @Context
    private HttpServletRequest request;
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(Comentarios comentario) {
        ejbComentariosFacade.create(comentario);
    }
    
    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") Integer id, Comentarios comentario) {
        ejbComentariosFacade.edit(comentario);
    }
    
    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        ejbComentariosFacade.remove(ejbComentariosFacade.find(id));
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Comentarios> findAll() {
        return ejbComentariosFacade.findAll();
    }
    
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Comentarios findById(@PathParam("id") Integer id) {
        return ejbComentariosFacade.find(id);
    }
    @GET
    @Path("byJardin/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Comentarios> findComentariosByIdJardin(@PathParam("id") Integer id) {
      return ejbComentariosFacade.findComentariosByIdJardin(ejbjardinesInfantilesFacade.find(id).getIdJardinInfantil());
    }
    
    @POST
    @Path("setComentario")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(Comentarios comentario) throws JOSEException  {
         GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        try{  
        comentario.setIdUsuarios(         
                    new Usuarios(
                    Integer.parseInt(
                            AuthUtils.getSubject(
                            request.getHeader(AuthUtils.AUTH_HEADER_KEY)))));
        comentario.setFecha(null);
                        ejbComentariosFacade.create(comentario);
         
         return Response
                    .status(Response.Status.OK)
                    .entity(gson.toJson("Comentario registrado correctamente"))
                    .build();
         
          }catch(ParseException | JOSEException | NumberFormatException ex){
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,null,ex);
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson("Error de persistencia")).build();
        }
    }
    
}
