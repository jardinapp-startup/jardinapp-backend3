package com.adsicbi.jardinapp.rest.services;


import com.adsicbi.jardinapp.jpa.entities.Inquietudes;
import com.adsicbi.jardinapp.jpa.entities.Usuarios;
import com.adsicbi.jardinapp.jpa.sessions.InquietudesFacade;
import com.adsicbi.jardinapp.rest.auth.AuthUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nimbusds.jose.JOSEException;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author andres
 */
@Path("inquietudes")
@Produces(MediaType.APPLICATION_JSON)
public class InquietudesRest {
    
    @EJB
    private InquietudesFacade ejbInquietudesFacade;
     @Context
    private HttpServletRequest request;
     
 
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(Inquietudes inquietudes) throws JOSEException {
        GsonBuilder gsonBuilder=new GsonBuilder();
        Gson gson=gsonBuilder.create();
        try{
            inquietudes.setIdUsuarios(         
                    new Usuarios(
                    Integer.parseInt(
                            AuthUtils.getSubject(
                            request.getHeader(AuthUtils.AUTH_HEADER_KEY)))));
               inquietudes.setFecha("");
               ejbInquietudesFacade.create(inquietudes);
               return Response.ok().entity(gson.toJson("Se creo la inquietud satisfactoriamente")).build();
        }catch(ParseException | JOSEException | NumberFormatException ex){
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,null,ex);
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson("Error de persistencia")).build();
        }
    }

    @GET
    @Path("byuser")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Inquietudes> findByidJardin() {
        try {
            return ejbInquietudesFacade.findByidJardin(Integer.parseInt(
                    AuthUtils.getSubject(
                            request.getHeader(AuthUtils.AUTH_HEADER_KEY))));
        } catch (ParseException | JOSEException ex) {
            Logger.getLogger(InquietudesRest.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Inquietudes findById(@PathParam("id") Integer id) {
        return ejbInquietudesFacade.find(id);
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Inquietudes> findAll() {
        return ejbInquietudesFacade.findAll();
    }
    
    @GET
    @Path("byusuarios")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Inquietudes> findByIdUsuario(){
        try{
            return ejbInquietudesFacade.findByIdUsuarios(Integer.parseInt(
                    AuthUtils.getSubject(
                      request.getHeader(AuthUtils.AUTH_HEADER_KEY))));
        }catch (ParseException | JOSEException ex){
            return null;
        }
    }
    
    
}
