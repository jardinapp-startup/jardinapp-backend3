/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.rest.services;

import com.adsicbi.jardinapp.jpa.entities.HijosAu;
import com.adsicbi.jardinapp.jpa.sessions.HijosAuFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author APRENDIZ
 */
@Path("hijosAu")
@Produces(MediaType.APPLICATION_JSON)
public class HijosAuRest {
    
     @EJB
    private HijosAuFacade hijosAuFacade;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<HijosAu> findAll() {
        return hijosAuFacade.findAll();
    }
    
}
