/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.rest.services;

import com.adsicbi.jardinapp.jpa.entities.TiposJardines;
import com.adsicbi.jardinapp.jpa.sessions.TiposJardinesFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Paola Andrea
 */
@Path("tiposjardines")
public class TiposJardinesRest {
    
     @EJB
    private TiposJardinesFacade ejbTiposJardinesFacade;  
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<TiposJardines> findAll() {
        return ejbTiposJardinesFacade.findAll();
    }  
   
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public TiposJardines findById(@PathParam("id") String id) {
        return ejbTiposJardinesFacade.find(id);
    }
    
    @GET
    @Path("nombre/{query}")
    public List<TiposJardines> findByNombre(@PathParam("query") String query){
        return ejbTiposJardinesFacade.findByNombre(query);
    } 
    
}
