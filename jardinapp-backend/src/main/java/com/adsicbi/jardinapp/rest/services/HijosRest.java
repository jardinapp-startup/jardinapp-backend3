/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.rest.services;

import com.adsicbi.jardinapp.jpa.entities.Hijos;
import com.adsicbi.jardinapp.jpa.entities.TiposDocumentos;
import com.adsicbi.jardinapp.jpa.entities.Usuarios;
import com.adsicbi.jardinapp.jpa.sessions.HijosFacade;
import com.adsicbi.jardinapp.jpa.sessions.UsuariosFacade;
import com.adsicbi.jardinapp.rest.auth.AuthUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nimbusds.jose.JOSEException;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author david
 */
@Path("hijos")
public class HijosRest {
    @EJB
    private HijosFacade ejbHijosFacade;
    
    @EJB
    private UsuariosFacade ejbUsuariosFacade;
    
    @Context
    private HttpServletRequest request;
    
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Hijos> findAll(){
    return ejbHijosFacade.findAll();
    }   
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(Hijos hijos) throws JOSEException {
        GsonBuilder gsonBuilder=new GsonBuilder();
        Gson gson=gsonBuilder.create();
        try{
            hijos.setIdUsuarios(         
                    new Usuarios(
                    Integer.parseInt(
                            AuthUtils.getSubject(
                            request.getHeader(AuthUtils.AUTH_HEADER_KEY)))));
               hijos.setIdTipoDocumento(
                     new TiposDocumentos(4));
               ejbHijosFacade.create(hijos);
               return Response.ok().entity(gson.toJson("Se creo la inquietud satisfactoriamente")).build();
        }catch(ParseException | JOSEException | NumberFormatException ex){
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,null,ex);
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson("Error de persistencia")).build();
        }
    }    
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("createAdminJardin")
    public Response createAdminJardin(Hijos hijos) throws JOSEException {
        GsonBuilder gsonBuilder=new GsonBuilder();
        Gson gson=gsonBuilder.create();
       
          
               hijos.setIdTipoDocumento(
                     new TiposDocumentos(4));
               ejbHijosFacade.create(hijos);
               return Response.ok().entity(gson.toJson("Se registro el niño satisfactoriamente")).build();
       
    }    
    
    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Path("{id}")
    public void edit(@PathParam("id")Integer id, Hijos hijos){
     ejbHijosFacade.edit(hijos);
}
    
    @PUT
    //@RolesAllowed({"adminP"})
    @Path("inhabilit/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response inhabilit(@PathParam("id") Integer id) {
         Hijos hijo=ejbHijosFacade.find(id);
         if(hijo.getEstado()){
             hijo.setEstado(Boolean.FALSE);
             
         }else{
             hijo.setEstado(Boolean.TRUE);
         }
        
         GsonBuilder gsonBuilder = new GsonBuilder();
         Gson gson = gsonBuilder.create();   
         
         try{
             ejbHijosFacade.edit(hijo);
             return Response.ok()
                     .entity(gson.toJson("El estado del hijo se cambio satisfactoriamente"))
                     .build();
         }catch(Exception ex){
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,null,ex);
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson("Error de persistencia")).build();
         }
   
    }
    
    @DELETE
    @Consumes({MediaType.APPLICATION_JSON})
    public void remove(@PathParam("id")Integer id, Hijos hijos){
     ejbHijosFacade.remove(ejbHijosFacade.find(id));       
}
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("{id}")
    public Hijos findById (@PathParam("id")Integer id){
         return ejbHijosFacade.find(id);
     }
    /*
    *Este API REST consulta solo a los hijos del usario que es Padre con rol usuario, es decir, solo a sus hijos
    */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("getMisHijos")
    public List<Hijos> getMisHijos () throws ParseException, JOSEException{
      Usuarios idUserLogeado =  ejbUsuariosFacade.find(Integer.parseInt(
                            AuthUtils.getSubject(
                            request.getHeader(AuthUtils.AUTH_HEADER_KEY))));
      
         return  ejbHijosFacade.getMisHijos(idUserLogeado.getIdUsuarios());
     }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("validaRegistro/{registroCivil}")
      public Response findUsuarioByDocumento(@PathParam("registroCivil") int registroCivil) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        if (ejbHijosFacade.findHijosByRegistroCivil(registroCivil) != null) {
            return Response 
                    .status(Response.Status.CONFLICT)
                    .entity(gson.toJson("false"))
                    .build();
        } else {

            return Response
                    .status(Response.Status.OK)
                    .entity(gson.toJson("true"))
                    .build();
        }
      }
    
    }