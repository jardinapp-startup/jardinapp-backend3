/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.rest.services;

/**
 *
 * @author PC 4
 */


import com.adsicbi.jardinapp.jpa.entities.Calificaciones;
import com.adsicbi.jardinapp.jpa.entities.Usuarios;
import com.adsicbi.jardinapp.jpa.sessions.CalificacionesFacade;
import com.adsicbi.jardinapp.rest.auth.AuthUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nimbusds.jose.JOSEException;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;



/**
 *
 * @author PC 4
 */
@Path("calificaciones")
@Produces(MediaType.APPLICATION_JSON)
public class CalificacionesRest {
    
    @EJB
    private CalificacionesFacade ejbCalificacionesFacade;

    @Context
    private HttpServletRequest request;

    @Path("create")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
     public Response create(Calificaciones calificaciones) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        try {
            calificaciones.setIdUsuarios(
                    new Usuarios(
                                 Integer.parseInt(
                                    AuthUtils.getSubject(
                                            request.getHeader(AuthUtils.AUTH_HEADER_KEY)))));
            calificaciones.setFecha("");
            ejbCalificacionesFacade.create(calificaciones);
            return Response.ok().entity(gson.toJson("La calificacion fue registrada exitosamente")).build();
        } catch (ParseException | JOSEException | NumberFormatException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson("Error de persistencia")).build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Calificaciones> findAll() {
        return ejbCalificacionesFacade.findAll();
    }
}
