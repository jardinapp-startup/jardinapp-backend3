/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsicbi.jardinapp.rest.services;

import com.adsicbi.jardinapp.jpa.entities.JardinesinfantilesAU;
import com.adsicbi.jardinapp.jpa.sessions.JardinesinfantilesAUFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author vbn
 */
@Path("jardinesinfantilesAU")
@Produces(MediaType.APPLICATION_JSON)
public class JardinesInfantilesAURest {
    
    @EJB
    private JardinesinfantilesAUFacade ejbJardinesinfantilesAUFacade;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<JardinesinfantilesAU> findAll() {
        return ejbJardinesinfantilesAUFacade.findAll();
    }
    
}
