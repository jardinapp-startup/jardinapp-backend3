package com.adsicbi.jardinapp.rest.services;

import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

/**
 *
 * @author leoandresm
 */
@ApplicationPath("webresources")
public class ApplicationConfig extends ResourceConfig {

    public ApplicationConfig() {
        packages("com.adsicbi.jardinapp.rest.services;com.adsicbi.jardinapp.rest.auth");
        register(RolesAllowedDynamicFeature.class);
    }

}
