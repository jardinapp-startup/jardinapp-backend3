package com.adsicbi.jardinapp.jpa.entities;

import com.adsicbi.jardinapp.jpa.entities.Anuncios;
import com.adsicbi.jardinapp.jpa.entities.Calificaciones;
import com.adsicbi.jardinapp.jpa.entities.Comentarios;
import com.adsicbi.jardinapp.jpa.entities.Hijos;
import com.adsicbi.jardinapp.jpa.entities.Inquietudes;
import com.adsicbi.jardinapp.jpa.entities.JardinesInfantilesHasUsuarios;
import com.adsicbi.jardinapp.jpa.entities.TiposJardines;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-15T11:45:52")
@StaticMetamodel(JardinesInfantiles.class)
public class JardinesInfantiles_ { 

    public static volatile SingularAttribute<JardinesInfantiles, String> descripcion;
    public static volatile SingularAttribute<JardinesInfantiles, Boolean> estado;
    public static volatile ListAttribute<JardinesInfantiles, Comentarios> comentariosList;
    public static volatile SingularAttribute<JardinesInfantiles, String> direccion;
    public static volatile ListAttribute<JardinesInfantiles, Calificaciones> calificacionesList;
    public static volatile ListAttribute<JardinesInfantiles, Inquietudes> inquietudesList;
    public static volatile ListAttribute<JardinesInfantiles, JardinesInfantilesHasUsuarios> jardinesInfantilesHasUsuariosList;
    public static volatile SingularAttribute<JardinesInfantiles, Integer> idJardinInfantil;
    public static volatile SingularAttribute<JardinesInfantiles, TiposJardines> idTipoJardines;
    public static volatile SingularAttribute<JardinesInfantiles, String> nombreJardin;
    public static volatile SingularAttribute<JardinesInfantiles, String> sitioweb;
    public static volatile SingularAttribute<JardinesInfantiles, byte[]> foto;
    public static volatile ListAttribute<JardinesInfantiles, Hijos> hijosList;
    public static volatile SingularAttribute<JardinesInfantiles, String> celular;
    public static volatile SingularAttribute<JardinesInfantiles, String> telefono;
    public static volatile ListAttribute<JardinesInfantiles, Anuncios> anunciosList;
    public static volatile SingularAttribute<JardinesInfantiles, String> email;

}