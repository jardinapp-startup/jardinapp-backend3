package com.adsicbi.jardinapp.jpa.entities;

import com.adsicbi.jardinapp.jpa.entities.CiudadesPK;
import com.adsicbi.jardinapp.jpa.entities.Departamentos;
import com.adsicbi.jardinapp.jpa.entities.Empresas;
import com.adsicbi.jardinapp.jpa.entities.Usuarios;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-15T11:45:52")
@StaticMetamodel(Ciudades.class)
public class Ciudades_ { 

    public static volatile ListAttribute<Ciudades, Usuarios> usuariosList;
    public static volatile SingularAttribute<Ciudades, String> nombreCiudad;
    public static volatile SingularAttribute<Ciudades, CiudadesPK> ciudadesPK;
    public static volatile SingularAttribute<Ciudades, Departamentos> departamentos;
    public static volatile ListAttribute<Ciudades, Empresas> empresasList;

}