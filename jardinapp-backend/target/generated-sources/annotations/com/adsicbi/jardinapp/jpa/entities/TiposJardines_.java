package com.adsicbi.jardinapp.jpa.entities;

import com.adsicbi.jardinapp.jpa.entities.JardinesInfantiles;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-15T11:45:52")
@StaticMetamodel(TiposJardines.class)
public class TiposJardines_ { 

    public static volatile SingularAttribute<TiposJardines, String> nombreTipoJardines;
    public static volatile SingularAttribute<TiposJardines, Integer> idTipoJardines;
    public static volatile ListAttribute<TiposJardines, JardinesInfantiles> jardinesInfantilesList;

}