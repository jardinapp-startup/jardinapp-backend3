package com.adsicbi.jardinapp.jpa.entities;

import com.adsicbi.jardinapp.jpa.entities.Ciudades;
import com.adsicbi.jardinapp.jpa.entities.Usuarios;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-15T11:45:52")
@StaticMetamodel(Empresas.class)
public class Empresas_ { 

    public static volatile SingularAttribute<Empresas, Boolean> estado;
    public static volatile SingularAttribute<Empresas, String> direccion;
    public static volatile SingularAttribute<Empresas, String> nit;
    public static volatile SingularAttribute<Empresas, String> representanteLegal;
    public static volatile SingularAttribute<Empresas, String> celular;
    public static volatile SingularAttribute<Empresas, Integer> idEmpresa;
    public static volatile SingularAttribute<Empresas, String> telefono;
    public static volatile SingularAttribute<Empresas, String> actividadEconomica;
    public static volatile SingularAttribute<Empresas, String> nombreEmpresa;
    public static volatile SingularAttribute<Empresas, String> email;
    public static volatile SingularAttribute<Empresas, Ciudades> ciudades;
    public static volatile SingularAttribute<Empresas, Usuarios> idUsuarios;

}