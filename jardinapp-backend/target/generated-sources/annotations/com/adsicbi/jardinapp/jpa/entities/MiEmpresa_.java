package com.adsicbi.jardinapp.jpa.entities;

import com.adsicbi.jardinapp.jpa.entities.Servicios;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-15T11:45:52")
@StaticMetamodel(MiEmpresa.class)
public class MiEmpresa_ { 

    public static volatile ListAttribute<MiEmpresa, Servicios> serviciosList;
    public static volatile SingularAttribute<MiEmpresa, Integer> nit;
    public static volatile SingularAttribute<MiEmpresa, String> direccion;
    public static volatile SingularAttribute<MiEmpresa, String> representanteLegal;
    public static volatile SingularAttribute<MiEmpresa, String> telefono;
    public static volatile SingularAttribute<MiEmpresa, Integer> idMiEmpresa;
    public static volatile SingularAttribute<MiEmpresa, String> nombreEmpresa;

}