package com.adsicbi.jardinapp.jpa.entities;

import com.adsicbi.jardinapp.jpa.entities.JardinesInfantiles;
import com.adsicbi.jardinapp.jpa.entities.Usuarios;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-15T11:45:52")
@StaticMetamodel(JardinesInfantilesHasUsuarios.class)
public class JardinesInfantilesHasUsuarios_ { 

    public static volatile SingularAttribute<JardinesInfantilesHasUsuarios, JardinesInfantiles> idJardinInfantil;
    public static volatile SingularAttribute<JardinesInfantilesHasUsuarios, Integer> idJardinesHasUsuarios;
    public static volatile SingularAttribute<JardinesInfantilesHasUsuarios, Usuarios> idUsuarios;

}