package com.adsicbi.jardinapp.jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-15T11:45:52")
@StaticMetamodel(JardinesinfantilesAU.class)
public class JardinesinfantilesAU_ { 

    public static volatile SingularAttribute<JardinesinfantilesAU, String> celularOld;
    public static volatile SingularAttribute<JardinesinfantilesAU, String> telefonoOld;
    public static volatile SingularAttribute<JardinesinfantilesAU, String> nombreJardinNew;
    public static volatile SingularAttribute<JardinesinfantilesAU, byte[]> fotoNew;
    public static volatile SingularAttribute<JardinesinfantilesAU, String> direccionOld;
    public static volatile SingularAttribute<JardinesinfantilesAU, String> sitiowebNew;
    public static volatile SingularAttribute<JardinesinfantilesAU, String> sitiowebOld;
    public static volatile SingularAttribute<JardinesinfantilesAU, String> estadoNew;
    public static volatile SingularAttribute<JardinesinfantilesAU, String> direccionNew;
    public static volatile SingularAttribute<JardinesinfantilesAU, String> estadoOld;
    public static volatile SingularAttribute<JardinesinfantilesAU, String> emailOld;
    public static volatile SingularAttribute<JardinesinfantilesAU, byte[]> fotoOld;
    public static volatile SingularAttribute<JardinesinfantilesAU, String> emailNew;
    public static volatile SingularAttribute<JardinesinfantilesAU, String> celularNew;
    public static volatile SingularAttribute<JardinesinfantilesAU, String> telefonoNew;
    public static volatile SingularAttribute<JardinesinfantilesAU, String> nombreJardinOld;
    public static volatile SingularAttribute<JardinesinfantilesAU, Integer> idTriggerJardines;

}