package com.adsicbi.jardinapp.jpa.entities;

import com.adsicbi.jardinapp.jpa.entities.JardinesInfantiles;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-15T11:45:52")
@StaticMetamodel(Anuncios.class)
public class Anuncios_ { 

    public static volatile SingularAttribute<Anuncios, String> descripcion;
    public static volatile SingularAttribute<Anuncios, Boolean> estado;
    public static volatile SingularAttribute<Anuncios, JardinesInfantiles> idJardinInfantil;
    public static volatile SingularAttribute<Anuncios, Date> fechaInicio;
    public static volatile SingularAttribute<Anuncios, String> titulo;
    public static volatile SingularAttribute<Anuncios, Integer> idAnuncio;
    public static volatile SingularAttribute<Anuncios, Date> fechaFin;

}