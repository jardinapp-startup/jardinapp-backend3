package com.adsicbi.jardinapp.jpa.entities;

import com.adsicbi.jardinapp.jpa.entities.JardinesInfantiles;
import com.adsicbi.jardinapp.jpa.entities.Usuarios;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-15T11:45:52")
@StaticMetamodel(Calificaciones.class)
public class Calificaciones_ { 

    public static volatile SingularAttribute<Calificaciones, Integer> idCalificaciones;
    public static volatile SingularAttribute<Calificaciones, Date> fecha;
    public static volatile SingularAttribute<Calificaciones, JardinesInfantiles> idJardinInfantil;
    public static volatile SingularAttribute<Calificaciones, Short> puntuacion;
    public static volatile SingularAttribute<Calificaciones, Usuarios> idUsuarios;

}