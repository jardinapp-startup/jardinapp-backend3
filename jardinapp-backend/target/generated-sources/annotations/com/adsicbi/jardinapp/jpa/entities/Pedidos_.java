package com.adsicbi.jardinapp.jpa.entities;

import com.adsicbi.jardinapp.jpa.entities.DetallesPedidos;
import com.adsicbi.jardinapp.jpa.entities.Usuarios;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-15T11:45:52")
@StaticMetamodel(Pedidos.class)
public class Pedidos_ { 

    public static volatile SingularAttribute<Pedidos, Date> fecha;
    public static volatile SingularAttribute<Pedidos, Boolean> estado;
    public static volatile ListAttribute<Pedidos, DetallesPedidos> detallesPedidosList;
    public static volatile SingularAttribute<Pedidos, Integer> idPedido;
    public static volatile SingularAttribute<Pedidos, Usuarios> idUsuarios;

}