package com.adsicbi.jardinapp.jpa.entities;

import com.adsicbi.jardinapp.jpa.entities.Calificaciones;
import com.adsicbi.jardinapp.jpa.entities.Ciudades;
import com.adsicbi.jardinapp.jpa.entities.Comentarios;
import com.adsicbi.jardinapp.jpa.entities.Empresas;
import com.adsicbi.jardinapp.jpa.entities.Encuestas;
import com.adsicbi.jardinapp.jpa.entities.Hijos;
import com.adsicbi.jardinapp.jpa.entities.Inquietudes;
import com.adsicbi.jardinapp.jpa.entities.JardinesInfantilesHasUsuarios;
import com.adsicbi.jardinapp.jpa.entities.Pedidos;
import com.adsicbi.jardinapp.jpa.entities.Roles;
import com.adsicbi.jardinapp.jpa.entities.TiposDocumentos;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-15T11:45:52")
@StaticMetamodel(Usuarios.class)
public class Usuarios_ { 

    public static volatile SingularAttribute<Usuarios, String> apellidos;
    public static volatile SingularAttribute<Usuarios, Boolean> estado;
    public static volatile ListAttribute<Usuarios, Comentarios> comentariosList;
    public static volatile SingularAttribute<Usuarios, String> direccion;
    public static volatile SingularAttribute<Usuarios, String> documento;
    public static volatile SingularAttribute<Usuarios, byte[]> avatar;
    public static volatile ListAttribute<Usuarios, Calificaciones> calificacionesList;
    public static volatile SingularAttribute<Usuarios, String> nombres;
    public static volatile ListAttribute<Usuarios, Inquietudes> inquietudesList;
    public static volatile ListAttribute<Usuarios, JardinesInfantilesHasUsuarios> jardinesInfantilesHasUsuariosList;
    public static volatile SingularAttribute<Usuarios, String> tituloProfesional;
    public static volatile SingularAttribute<Usuarios, String> password;
    public static volatile ListAttribute<Usuarios, Roles> rolesList;
    public static volatile ListAttribute<Usuarios, Hijos> hijosList;
    public static volatile SingularAttribute<Usuarios, TiposDocumentos> idTipoDocumento;
    public static volatile SingularAttribute<Usuarios, Encuestas> idEncuesta;
    public static volatile SingularAttribute<Usuarios, String> celular;
    public static volatile ListAttribute<Usuarios, Empresas> empresasList;
    public static volatile SingularAttribute<Usuarios, String> telefono;
    public static volatile ListAttribute<Usuarios, Pedidos> pedidosList;
    public static volatile SingularAttribute<Usuarios, Integer> idUsuarios;
    public static volatile SingularAttribute<Usuarios, String> email;
    public static volatile SingularAttribute<Usuarios, Ciudades> ciudades;

}