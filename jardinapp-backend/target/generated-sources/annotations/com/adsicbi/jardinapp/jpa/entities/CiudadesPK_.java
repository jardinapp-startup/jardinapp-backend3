package com.adsicbi.jardinapp.jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-15T11:45:52")
@StaticMetamodel(CiudadesPK.class)
public class CiudadesPK_ { 

    public static volatile SingularAttribute<CiudadesPK, Integer> idDepartamento;
    public static volatile SingularAttribute<CiudadesPK, Integer> idCiudad;

}