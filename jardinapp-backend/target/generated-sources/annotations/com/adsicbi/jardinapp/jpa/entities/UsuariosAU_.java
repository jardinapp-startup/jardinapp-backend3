package com.adsicbi.jardinapp.jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-15T11:45:52")
@StaticMetamodel(UsuariosAU.class)
public class UsuariosAU_ { 

    public static volatile SingularAttribute<UsuariosAU, String> celularOld;
    public static volatile SingularAttribute<UsuariosAU, String> telefonoOld;
    public static volatile SingularAttribute<UsuariosAU, String> passwordOld;
    public static volatile SingularAttribute<UsuariosAU, String> tituloProfesionalOld;
    public static volatile SingularAttribute<UsuariosAU, String> direccionOld;
    public static volatile SingularAttribute<UsuariosAU, String> apellidosOld;
    public static volatile SingularAttribute<UsuariosAU, String> passwordNew;
    public static volatile SingularAttribute<UsuariosAU, Integer> documentoOld;
    public static volatile SingularAttribute<UsuariosAU, String> direccionNew;
    public static volatile SingularAttribute<UsuariosAU, Integer> documentoNew;
    public static volatile SingularAttribute<UsuariosAU, String> apellidosNew;
    public static volatile SingularAttribute<UsuariosAU, String> emailOld;
    public static volatile SingularAttribute<UsuariosAU, String> nombresOld;
    public static volatile SingularAttribute<UsuariosAU, String> emailNew;
    public static volatile SingularAttribute<UsuariosAU, String> telefonoNew;
    public static volatile SingularAttribute<UsuariosAU, String> celularNew;
    public static volatile SingularAttribute<UsuariosAU, String> nombresNew;
    public static volatile SingularAttribute<UsuariosAU, Integer> idUsuarios;
    public static volatile SingularAttribute<UsuariosAU, String> tituloProfesionalNew;

}