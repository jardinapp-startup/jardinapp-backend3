package com.adsicbi.jardinapp.jpa.entities;

import com.adsicbi.jardinapp.jpa.entities.DetallesPedidosPK;
import com.adsicbi.jardinapp.jpa.entities.Pedidos;
import com.adsicbi.jardinapp.jpa.entities.Servicios;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-15T11:45:52")
@StaticMetamodel(DetallesPedidos.class)
public class DetallesPedidos_ { 

    public static volatile SingularAttribute<DetallesPedidos, Servicios> servicios;
    public static volatile SingularAttribute<DetallesPedidos, DetallesPedidosPK> detallesPedidosPK;
    public static volatile SingularAttribute<DetallesPedidos, Date> fechaInicio;
    public static volatile SingularAttribute<DetallesPedidos, Integer> duracionMeses;
    public static volatile SingularAttribute<DetallesPedidos, Pedidos> pedidos;
    public static volatile SingularAttribute<DetallesPedidos, Date> fechaFin;

}