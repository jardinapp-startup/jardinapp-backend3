package com.adsicbi.jardinapp.jpa.entities;

import com.adsicbi.jardinapp.jpa.entities.JardinesInfantiles;
import com.adsicbi.jardinapp.jpa.entities.TiposDocumentos;
import com.adsicbi.jardinapp.jpa.entities.Usuarios;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-15T11:45:52")
@StaticMetamodel(Hijos.class)
public class Hijos_ { 

    public static volatile SingularAttribute<Hijos, String> apellidos;
    public static volatile SingularAttribute<Hijos, Integer> idHijo;
    public static volatile SingularAttribute<Hijos, String> segundoNombre;
    public static volatile SingularAttribute<Hijos, Boolean> estado;
    public static volatile SingularAttribute<Hijos, JardinesInfantiles> idJardinInfantil;
    public static volatile SingularAttribute<Hijos, String> primerNombre;
    public static volatile SingularAttribute<Hijos, TiposDocumentos> idTipoDocumento;
    public static volatile SingularAttribute<Hijos, Integer> registroCivil;
    public static volatile SingularAttribute<Hijos, Usuarios> idUsuarios;

}