package com.adsicbi.jardinapp.jpa.entities;

import com.adsicbi.jardinapp.jpa.entities.JardinesInfantiles;
import com.adsicbi.jardinapp.jpa.entities.RespuestasInquietudes;
import com.adsicbi.jardinapp.jpa.entities.Usuarios;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-15T11:45:52")
@StaticMetamodel(Inquietudes.class)
public class Inquietudes_ { 

    public static volatile SingularAttribute<Inquietudes, String> descripcion;
    public static volatile SingularAttribute<Inquietudes, Date> fecha;
    public static volatile SingularAttribute<Inquietudes, JardinesInfantiles> idJardinInfantil;
    public static volatile SingularAttribute<Inquietudes, Integer> idInquietud;
    public static volatile SingularAttribute<Inquietudes, String> asunto;
    public static volatile ListAttribute<Inquietudes, RespuestasInquietudes> respuestasInquietudesList;
    public static volatile SingularAttribute<Inquietudes, Usuarios> idUsuarios;

}